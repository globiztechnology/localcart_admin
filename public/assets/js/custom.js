$(document).ready(function () {

    function fetch_data(page, sort_type, sort_by, query,type) {
        $.ajax({
            beforeSend: function () {
                $('.ajax-loader').css("visibility", "visible");
            },
            url: location.origin + "/customers/fetch-data?page=" + page + "&sortby=" + sort_by + "&sorttype=" + sort_type + "&query=" + query + "&type="+type,
            success: function (data) {
                $('tbody').html('');
                $('tbody').html(data);
            },
            complete: function () {
                $('.ajax-loader').css("visibility", "hidden");
            }
        });
    }
    $(document).on('keyup', '#search-user', function () {
        var query = $(this).val();
        var column_name = $('#hidden_column_name').val();
        var sort_type = $('#hidden_sort_type').val();
        var page = $('#hidden_page').val();
        var type = $('#hidden_user_type').val();
        fetch_data(page, sort_type, column_name, query, type);
    })
    $(document).on('click', '.sorting', function () {
        var column_name = $(this).data('column-name');
        var order_type = $(this).data('sorting-type');
        var reverse_order = '';
        if (order_type == 'asc') {
            $(this).data('sorting-type', 'desc');
            reverse_order = 'desc';
            $('#' + column_name + '_icon').html('<span class="fa fa-caret-down"></span>');
        }
        if (order_type == 'desc') {
            $(this).data('sorting-type', 'asc');
            reverse_order = 'asc';
            $('#' + column_name + '_icon').html('<span class="fa fa-caret-up"></span>')
        }
        $('#hidden_column_name').val(column_name);
        $('#hidden_sort_type').val(reverse_order);
        var query = $('#search-user').val();
        var page = $('#hidden_page').val();
        var type = $('#hidden_user_type').val();
        fetch_data(page, reverse_order, column_name, query, type)
    })
    $(document).on('click', '.paging-users .pagination a', function (event) {
        event.preventDefault();
        var page = $(this).attr('href').split('page=')[1];
        $('#hidden_page').val(page);
        var column_name = $('#hidden_column_name').val();
        var sort_type = $('#hidden_sort_type').val();
        var query = $('#search-user').val();
        var type = $('#hidden_user_type').val();
        console.log(column_name + " " + sort_type);
        fetch_data(page, sort_type, column_name, query,type);
    });
    $('.delete-user').on('click', function () {
        var user_id = $(this).attr('rel');
        var type = $(this).data('type');
        swal({
            title: "Are you sure?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: "delete-user",
                        type: "POST",
                        data: {
                            user_id,
                            type
                        },
                        success: function (data) {
                            if (data.status) {
                                swal(data.message, {
                                    icon: "success",
                                });
                                location.reload();
                            } else {
                                swal(data.message, {
                                    icon: "error"
                                });
                            }
                        }

                    });

                } else {
                    swal("Your data is safe!");
                }
            });
    });
    $('.delete-item').on('click', function() {
        $key = $(this).data('key');
        $id = $(this).attr('rel');
        swal({
                title: "Are you sure?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: "delete?key=" + $key + "&id=" + $id,
                        success: function(data) {
                            if (data.status) {
                                swal(data.message, {
                                    icon: "success",
                                });
                                location.reload();
                            } else {
                                swal(data.message, {
                                    icon: "error"
                                });
                            }
                        }

                    });

                } else {
                    swal("Your data is safe!");
                }
            });
    })

    $('#search-country').on('keyup',function(){
        var query = $(this).val();
        $.ajax({
            beforeSend: function () {
            $('.ajax-loader').css("visibility", "visible");
            },
            url: location.origin + "/countries?query=" + query ,
            success: function (data) {
                $('tbody').html('');
                $('tbody').html(data);
            },
            complete: function () {
                $('.ajax-loader').css("visibility", "hidden");
            }
        });
    });

    $('#search-state').on('keyup',function(){
        var query = $(this).val();
        $.ajax({
            beforeSend: function () {
            $('.ajax-loader').css("visibility", "visible");
            },
            url: location.origin + "/states?query=" + query ,
            success: function (data) {
                $('tbody').html('');
                $('tbody').html(data);
            },
            complete: function () {
                $('.ajax-loader').css("visibility", "hidden");
            }
        });
    });
    $('#search-city').on('keyup',function(){
        var query = $(this).val();
        $.ajax({
            beforeSend: function () {
            $('.ajax-loader').css("visibility", "visible");
            },
            url: location.origin + "/districts?query=" + query ,
            success: function (data) {
                $('tbody').html('');
                $('tbody').html(data);
            },
            complete: function () {
                $('.ajax-loader').css("visibility", "hidden");
            }
        });
    });

    $('#edit-category').bootstrapValidator({
        framework: 'bootstrap',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        err: {
            container: 'popover'
        },
        fields: {
            modal_edit_category_name: {
                validators: {
                    notEmpty: {
                        message: "Please enter category name."
                    }
                }
            }
        }
    });

    $('#add-category').bootstrapValidator({
        framework: 'bootstrap',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        err: {
            container: 'popover'
        },
        fields: {
            modal_category_name: {
                validators: {
                    notEmpty: {
                        message: "Please enter category name."
                    }
                }
            }
        }
    });

    $('.delete-category').on('click', function() {
        var category_id = $(this).attr('rel');
        swal({
                title: "Are you sure?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: "delete-category",
                        type: "POST",
                        data: {
                            category_id
                        },
                        success: function(data) {
                            if (data.status) {
                                swal(data.message, {
                                    icon: "success",
                                });
                                location.reload();
                            } else {
                                swal(data.message, {
                                    icon: "error"
                                });
                            }
                        }

                    });

                } else {
                    swal("Your data is safe!");
                }
            });
    });

    $('.edit-category').on('click', function() {
        $('#modal_edit_category_id').val($(this).attr('rel'));
        $('#modal_edit_category_name').val($(this).attr('data-category'));
    });

    $('#edit-country').bootstrapValidator({
        framework: 'bootstrap',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        err: {
            container: 'popover'
        },
        fields: {
            country: {
                validators: {
                    notEmpty: {
                        message: "Please enter country name!!"
                    }
                }
            }
        }
    });

    $('#edit-state').bootstrapValidator({
        framework: 'bootstrap',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        err: {
            container: 'popover'
        },
        fields: {
            state: {
                validators: {
                    notEmpty: {
                        message: "Please enter country name!!"
                    }
                }
            }
        }
    });

    $('.edit-order').on('click', function() {
        $('#modal_edit_order_status').val($(this).data('status'));
        $('#modal_edit_order_id').val($(this).attr('rel'));
        $('#modal_edit_order_status').select2().val($(this).data('status'));
    });

    $('.delete-order').on('click', function() {
        var order_id = $(this).attr('rel');
        swal({
                title: "Are you sure?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: "delete-order",
                        type: "POST",
                        data: {
                            order_id
                        },
                        success: function(data) {
                            if (data.status) {
                                swal(data.message, {
                                    icon: "success",
                                });
                                location.reload();
                            } else {
                                swal(data.message, {
                                    icon: "error"
                                });
                            }
                        }

                    });

                } else {
                    swal("Your data is safe!");
                }
            });
    });

});