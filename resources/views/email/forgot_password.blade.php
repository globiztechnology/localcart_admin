<!DOCTYPE html>
<html>
<head>
	<title>Forgot Password</title>
</head>
<body style="font-family:arial">
<table cellspacing="0" cellpadding="0" width="650px" style="padding: 0;border-collapse:collapse; margin: 0 auto;" border="0">
	<tbody style="background-color:#ba0001;">
		<tr>
			<td>
				<table cellspacing="0" cellpadding="0" width="580px" style="padding: 0;border-collapse:collapse; margin: 35px 15px 35px 35px; "border="0">
					<tbody style="background-color:#fff; border-bottom:1px solid #eee; text-align:center">
						<tr>
							<td style="text-align:center">
								<img src="{{url('./public/images/logo.png')}}" style="width:250px; height:auto;margin:0 auto;display:inline-block;margin-top: 22px;">
							</td>
						</tr>
						<tr style="height:50px">
							<td>
								<span style="font-size:24px; color:#784f1d;">Forgot Password</span>
							</td>
						</tr>
						<tr>
							<td>
								<p style="padding: 0 30px; margin-top:8px; line-height:22px; font-size:14px;text-align: center;">
									<b>Dear, <?php echo $name; ?></b>
							
								</p>
								<p style="padding: 0 30px; margin-top:8px; line-height:22px; font-size:14px;text-align: center;">
									It look like you requested a new password.
									<br>
									If thats sounds right, you can enter new password by clicking on the link below.
								</p>
									<a style="color: blue;" href="{{url('reset-password')}}/<?php echo $id; ?>/<?php echo $random_number;?>">Reset Password</a>
							</td>	
						</tr>
						
						<tr>
							<td colspan="3" style="text-align: center;">
								<p>Thank You</p>
							</td>
						</tr>
						<tr >
							<td>
								<?php $year  =  date('Y');?>
								<p style="font-size:14px; color:#784f1d;">Copyright &#9400; {{$year}} Localcart. All Right Reserved</p>
							</td>
						</tr>
					</tbody>	
				</table>
			</td>
		</tr>
	</tbody>	
</table>
</body>
</html>