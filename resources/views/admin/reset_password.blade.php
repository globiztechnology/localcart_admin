


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Localcart Forgot Password</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- ==== Font Awesome ==== -->
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.11.0/css/all.css">
    <!-- ==== Ionicons ==== -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- ==== iCheck ==== -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/icheck-bootstrap@3.0.1/icheck-bootstrap.min.css">
    <!-- ==== Theme style ==== -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/admin-lte@3.0.1/dist/css/adminlte.min.css">
    <!-- ==== Google Font: Source Sans Pro ==== -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link rel="stylesheet" href="https://globizdevserver.com/autopsy//assets/backend/dist/css/custom.css">
    <style>
        p{
            font-weight: 500; 
            color: #000;
        }
        a{
            font-weight: 500;
        }
        .code-area{
            text-align: center;
        }
        .login-page{
            height: 90vh;
        }
        .callout p{
            color: #fff;
        }
    </style>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
</head>
<body class="hold-transition login-page">
    <div class="login-box" style="max-width: 500px; width: 500px;">
        <div class="login-logo">
            <a href="{{url('/')}}" target="_blank"><img src="{{url('./public/images/logo.png')}}" width="60%"></a>
        </div>
        <div class="card">
            <div class="card-body login-card-body">
                
                <p class="login-box-msg">Reset Password</p>
                <form action="{{url('update-password')}}" class="form-signin" method="post" id="reset-form">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="random_number" value="<?php echo $random_number;?>">
                    <input type="hidden" name="admin_id" value="<?php echo $admin_id;?>">
                    <div class="account-logo">
                        <a href="index-2.html"><img src="assets/img/logo-dark.png" alt=""></a>
                    </div>
                    <div class="form-group">
                        <label>New Password <span class="custom-required-fields">*</span></label>
                        <input type="text"  name="password" autofocus="" class="form-control" placeholder="Enter New Password">
                    </div>
                    <div class="form-group">
                        <label>Confirm Password <span class="custom-required-fields">*</span></label>
                        <input type="text" name="confirm_password" class="form-control" placeholder="Enter Confirm Password">
                    </div>
                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-primary account-btn">Reset</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- ==== jQuery ==== -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!-- ==== Bootstrap ==== -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
    <!-- ==== AdminLTE App ==== -->
    <script src="https://cdn.jsdelivr.net/npm/admin-lte@3.0.1/dist/js/adminlte.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.5.3/js/bootstrapValidator.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#reset-form").bootstrapValidator({ 
                framework: 'bootstrap',
                feedbackIcons:{
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                    },
                    err: 
                    {
                        container: 'popover'
                    },
                    fields:{
                        password:{
                            validators:{
                                notEmpty:{
                                    message:"Please enter password !!"
                                }
                            }
                        }, 
                        confirm_password:{
                            validators:{
                                notEmpty:{
                                    message:"Please enter confirm password !!"
                                },
                                identical: {
                                    field: 'password',
                                    message: "Please enter same password !!"
                                },
                            }
                        },   
                    }
            });
        });
    </script> 
</body>
</html>