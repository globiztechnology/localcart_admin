@section('title','Orders')
@extends('layouts.adminlayout')
@section('content')
<div class="wrapper">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Orders</h1>
                    </div>
                </div>
            </div>
        </div>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="input-group">
                            <input class="form-control" id="search-order" style="float:right;" type="text" placeholder="Search Orders">
                            <span class="input-group-btn">
                            </span>
                        </div>
                    </div>
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                            <th>Customer Name</th>
                                            <th>Customer No.</th>
                                            <th>Shop Name</th>
                                            <th>Store Owner Name</th>
                                            <th>Store Owner No.</th>
                                            <th>Notes</th>
                                            <th>Pick Up Time</th>
                                            <th>Status</th>
                                            <th>Actions</th>
                                        </thead>
                                        <tbody>
                                            @include('admin.tables.orders_data')
                                        </tbody>

                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<div id="modal-edit-orders" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Change Order Status</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>

            </div>
            <div class="modal-body">
                <form id="edit-category" method="post" action="{{url('/edit-order-status')}}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="modal_edit_order_id" id="modal_edit_order_id">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <label class="col-md-12 col-xs-12 col-sm-12">Order Status</label>
                            <!-- <input type="text" class="form-control" id="modal_edit_order_status" name="modal_edit_order_status" /> -->
                            <select class="form-control" id="modal_edit_order_status" name="modal_edit_order_status">
                                <option value="rejected">rejected</option>
                                <option value="pending">pending</option>
                                <option value="accepted">accepted</option>
                                <option value="prepared">prepared</option>
                                <option value="completed">completed</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <input class="btn btn-primary" type="submit" value="submit" />
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>


<script>
    $(document).ready(function() {
        function fetch_order_data(query) {
            
            $.ajax({
                beforeSend: function() {
                    $('.ajax-loader').css("visibility", "visible");
                },
                url: location.origin + "/orders?query=" + query,
                success: function(data) {
                    $('tbody').html('');
                    $('tbody').html(data);
                },
                complete: function() {
                    $('.ajax-loader').css("visibility", "hidden");
                }
            });
        }
        $('#search-order').on('keyup', function() {
            var query = $(this).val();
            fetch_order_data(query);
        });
    });
</script>

@stop