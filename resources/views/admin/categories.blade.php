@section('title','Categories')
@extends('layouts.adminlayout')
@section('content')
<div class="wrapper">   
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark">Categories</h1>
                        </div>
                        <div class="col-sm-6">
                            <a href="" data-toggle="modal" data-target="#add-new-category" style="float:right; padding:.8rem; border: 1px solid black; border-radius:6px;">
                                <span class="fa fa-plus"> New Category</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="card">
                                <div class="card-body">
                                    <table class="table">
                                        <thead>
                                            <th>S.No.</th>
                                            <th>Category Name</th>
                                            <th>Actions</th>
                                        </thead>
                                        <tbody>
                                            @foreach($data as $key => $row)
                                                <tr>
                                                    <td>{{++$key}}</td>
                                                    <td>{{$row->category_name}}</td>
                                                    <td>
                                                        <a href="javascript:;" data-toggle="modal" data-target="#modal-edit-category" class="edit-category" rel="{{$row->id}}" data-category="{{$row->category_name}}">
                                                            <span class="fa fa-edit"></span>&nbsp;&nbsp;
                                                        </a>
                                                        
                                                        <a href="javascript:;"  class="delete-category" rel="{{$row->id}}">
                                                            <span class="fa fa-trash"></span>
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            
                                        </tbody>
                                        {{ $data->links() }}
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    <div id="add-new-category" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add New Category</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                
            </div>
            <div class="modal-body">
                <form id="add-category" method="post" action="{{url('/categories')}}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <label class="col-md-12 col-xs-12 col-sm-12">Category Name</label>
                            <input type="text" class="form-control" id="modal-category-name" name="modal_category_name" />
                        </div>
                    </div> 
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <input class="btn btn-primary" type="submit" value="submit" />
                        </div>
                    </div>      
                </form>
            </div>
            <!-- <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div> -->
            </div>

        </div>
    </div>

    <div id="modal-edit-category" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit Category</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                
            </div>
            <div class="modal-body">
                <form id="edit-category" method="post" action="{{url('/update-category')}}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="modal_edit_category_id" id="modal_edit_category_id" >
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <label class="col-md-12 col-xs-12 col-sm-12">Category Name</label>
                            <input type="text" class="form-control" id="modal_edit_category_name" name="modal_edit_category_name" />
                        </div>
                    </div> 
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <input class="btn btn-primary" type="submit" value="submit" />
                        </div>
                    </div>      
                </form>
            </div>
            <!-- <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div> -->
            </div>

        </div>
    </div>

    <script>
        $(document).ready(function(){
            $('.delete-category').on('click',function(){
                var category_id = $(this).attr('rel');
                swal({
                title: "Are you sure?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: "delete-category",
                            type: "POST",
                            data: {category_id},
                            success: function(data){
                                if(data.status){
                                    swal(data.message,{
                                        icon: "success",
                                    });
                                    location.reload();
                                }
                                else{
                                    swal(data.message,{
                                        icon: "error"
                                    });
                                }
                            }
                            
                        });
                        
                    } else {
                        swal("Your data is safe!");
                    }
                });
            });
        });
    </script>

    <script>
        $(document).ready(function(){
            $('#add-category').bootstrapValidator({
                    framework: 'bootstrap',
                    feedbackIcons:
                    {
                        valid: 'glyphicon glyphicon-ok',
                        invalid: 'glyphicon glyphicon-remove',
                        validating: 'glyphicon glyphicon-refresh'
                    },
                    err: 
                    {
                        container: 'popover'
                    },
                    fields:
                    {
                        modal_category_name:
                        {
                            validators:
                            {
                                notEmpty: 
                                {
                                    message: "Please enter category name."
                                }
                            }
                        }
                    } 
            });
        });
    </script>
    <script>
        $(document).ready(function(){
            $('#edit-category').bootstrapValidator({
                    framework: 'bootstrap',
                    feedbackIcons:
                    {
                        valid: 'glyphicon glyphicon-ok',
                        invalid: 'glyphicon glyphicon-remove',
                        validating: 'glyphicon glyphicon-refresh'
                    },
                    err: 
                    {
                        container: 'popover'
                    },
                    fields:
                    {
                        modal_edit_category_name:
                        {
                            validators:
                            {
                                notEmpty: 
                                {
                                    message: "Please enter category name."
                                }
                            }
                        }
                    } 
            });
        });
    </script>
    <script>
        $(document).ready(function(){
            $('.edit-category').on('click',function(){
                $('#modal_edit_category_id').val($(this).attr('rel'));
                $('#modal_edit_category_name').val($(this).attr('data-category'));
            });
        });
    </script>

    
@stop