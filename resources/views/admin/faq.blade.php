@section('title','FAQ')
@extends('layouts.adminlayout')
@section('content')


<div class="wrapper">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">FAQ's</h1>
                    </div>
                    <div class="col-sm-6">
                        <a href="" data-toggle="modal" data-target="#modal-add-faq" style="float:right; padding:.8rem; border: 1px solid black; border-radius:6px;">
                            Add Faq
                        </a>
                    </div>

                </div>
            </div>
        </div>
        <section class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>
                                                    Question
                                                </th>
                                                <th>
                                                    Answer
                                                </th>

                                                <th>Status</th>
                                                <th>Actions</th>
                                            </tr>

                                        </thead>
                                        <tbody>
                                            @include('admin.tables.faqs_data')
                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<div id="modal-add-faq" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add Faq</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>

            </div>
            <div class="modal-body">
                <form id="add-faq" method="post" action="{{url('/faq/add')}}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <label class="col-md-12 col-xs-12 col-sm-12">Question</label>
                            <input type="text" class="form-control" id="add_question" name="add_question" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <label class="col-md-12 col-xs-12 col-sm-12">Answer</label>
                            <textarea style="height:200" class="form-control" id="add_answer" name="add_answer"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <input class="btn btn-primary" type="submit" value="submit" />
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>

<div id="modal-edit-faq" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit Faq</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>

            </div>
            <div class="modal-body">
                <form id="edit-faq" method="post" action="{{url('/faq')}}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="faq_id" id="faq_id">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <label class="col-md-12 col-xs-12 col-sm-12">Question</label>
                            <input type="text" class="form-control" id="question" name="question" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <label class="col-md-12 col-xs-12 col-sm-12">Answer</label>
                            <textarea style="height:200" class="form-control" id="answer" name="answer"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <input class="btn btn-primary" type="submit" value="submit" />
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
<script>
    $(document).ready(function() {
        $("#add-faq").bootstrapValidator({
            framework: 'bootstrap',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            err: {
                container: 'popover'
            },
            fields: {
                add_question: {
                    validators: {
                        notEmpty: {
                            message: "Please enter question!!"
                        }
                    }
                },
                add_answer: {
                    validators: {
                        notEmpty: {
                            message: "Please enter answer!!"
                        }
                    }
                }
            }
        });
        $("#edit-faq").bootstrapValidator({
            framework: 'bootstrap',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            err: {
                container: 'popover'
            },
            fields: {
                question: {
                    validators: {
                        notEmpty: {
                            message: "Please enter question!!"
                        }
                    }
                },
                answer: {
                    validators: {
                        notEmpty: {
                            message: "Please enter answer!!"
                        }
                    }
                }
            }
        });

        $('.convert-faq-to-modal').on('click', function() {
            $('#faq_id').val($(this).attr('rel'));
            $('#question').val($(this).data('question'));
            $('#answer').val($(this).data('answer'));
        });

        $('.delete-faq').on('click', function() {
            $key = $(this).data('key');
            $id = $(this).attr('rel');
            swal({
                    title: "Are you sure?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: "faq/delete?" + "faq_id=" + $id,
                            success: function(data) {
                                if (data.status) {
                                    swal(data.message, {
                                        icon: "success",
                                    });
                                    location.reload();
                                } else {
                                    swal(data.message, {
                                        icon: "error"
                                    });
                                }
                            }

                        });

                    } else {
                        swal("Your data is safe!");
                    }
                });
        })
    });
</script>

@stop