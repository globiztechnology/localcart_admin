@section('title','District')
@extends('layouts.adminlayout')
@section('content')
<div class="ajax-loader">
    <img src="../images/loader1.gif" />
</div>
<div class="wrapper">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">District Listing</h1>
                    </div>
                </div>
            </div>
        </div>
        <section class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-xl-12">
                        <div class="input-group">
                            <input class="form-control" id="search-city" style="float:right;" type="text" placeholder="Search Country">
                            <span class="input-group-btn">
                            </span>
                        </div>
                    </div>
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>
                                                    S.No.
                                                </th>
                                                <th>
                                                    District
                                                </th>
                                                <th>
                                                    State
                                                </th>
                                                <th>
                                                    Country
                                                </th>
                                                <th>
                                                    Actions
                                                </th>
                                            </tr>

                                        </thead>
                                        <tbody>
                                            @include('admin.tables.cities_data')
                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

@stop