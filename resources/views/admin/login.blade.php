
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Localcart Login</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- ==== Font Awesome ==== -->
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.11.0/css/all.css">
    <!-- ==== Ionicons ==== -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- ==== iCheck ==== -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/icheck-bootstrap@3.0.1/icheck-bootstrap.min.css">
    <!-- ==== Theme style ==== -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/admin-lte@3.0.1/dist/css/adminlte.min.css">

    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <!-- ==== Google Font: Source Sans Pro ==== -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link rel="stylesheet" href="https://globizdevserver.com/autopsy//assets/backend/dist/css/custom.css">
    <!-- <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"> -->

    

        

    <style>
        p{
            font-weight: 500; 
            color: #000;
        }
        a{
            font-weight: 500;
        }
        .code-area{
            text-align: center;
        }
        .login-page{
            height: 90vh;
        }
        .callout p{
            color: #fff;
        }
        div  .has-error  small{
            color: red;
        }
    </style>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
</head>
<body class="hold-transition login-page" style="background-color: black;">
    <div class="login-box">
        <div class="login-logo" style="margin-bottom: 0;">
            <img src="{{$logo['value']}}" width="60%">
        </div>
        <div class="card">
            <div class="card-body login-card-body">
                
                <p class="login-box-msg">Sign in to start your session</p>
                <form action="{{url('/login')}}" id="login-form" method="post">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" name="email" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" name="password" placeholder="Password">
                    </div>
                    
                    <!-- <div class="input-group mb-3">
                        <>
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div> -->
                    <!-- <div class="form-group">
                        <div class="g-recaptcha" data-sitekey=""></div>
                    </div> -->
                    <div class="row">
                        <div class="col-8">
                            <!-- <div class="icheck-primary">
                                <input type="checkbox" id="remember">
                                <label for="remember">Remember Me</label>
                            </div> -->
                            <a href="{{url('/forgot-password')}}">Forgot Password</a>
                        </div>
                        <div class="col-4">
                            <input type="submit" value="Sign In" class="btn btn-primary btn-block">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
    <!-- ==== jQuery ==== -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!-- ==== Bootstrap ==== -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.5.3/js/bootstrapValidator.js"></script>
    <!-- ==== AdminLTE App ==== -->
    <script src="https://cdn.jsdelivr.net/npm/admin-lte@3.0.1/dist/js/adminlte.min.js"></script>
    

    <script src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.2/js/toastr.min.js"></script>

        <script>
            @if(Session::has('success'))
                $(function () {
                    toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": true,
                    "positionClass": "toast-top-right",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                    };
                    toastr.success("{{ Session::get('success') }}");
                });
            @endif    
        </script>
        <script>
            @if(Session::has('error'))
                $(function () {
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": true,
                    "positionClass": "toast-top-right",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                };
                toastr.error("{{ Session::get('error') }}");
                });
            @endif    
        </script>

    
    <script type="text/javascript">
        $(document).ready(function() {
            $("#login-form").bootstrapValidator({ 
                framework: 'bootstrap',
                feedbackIcons:{
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                    },
                    err: 
                    {
                        container: 'popover'
                    },
                    fields:{
                        email:{
                            validators:{
                                notEmpty:{
                                    message:"Please enter Email!!"
                                }
                            }
                        }, 
                        password:{
                            validators:{
                                notEmpty:{
                                    message:"Please enter password !!"
                                }
                            }
                        },   
                    }
            });
        });
    </script>
</body>
</html>