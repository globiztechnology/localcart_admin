@section('title','Edit Customer')
@extends('layouts.adminlayout')
@section('content')
<style>
    .card{
        padding:10px;
    }
</style>
<div class="wrapper">
    <div class="content-wrapper">
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12 col-sm-8">
                        <div class="card">
                            <div class="header">
                                <h2>Edit User</h2>
                            </div>

                            <div class="tab-content body">
                                <div role="tabpanel" class="tab-pane active" id="profile">
                                    <form id="form-validation" method="post">
                                        <div class="form-group">
                                            <label for="email_address">Name</label>
                                            <div class="form-line">
                                                <input type="text" id="name" name="name" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="email_address">Email</label>
                                            <div class="form-line">
                                                <input type="email" name="email" id="email" class="form-control">
                                            </div>

                                        </div>
                                        <div class="form-group">
                                            <label for="email_address">Phone Number</label>
                                            <div class="form-line">
                                                <input type="text" name="phonenumber" id="phonenumber" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Gender</label>
                                            <select type="text" id="gender" name="gender" class="form-control">
                                                <option value="male">Male</option>
                                                <option value="female">Female</option>
                                            </select>

                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Address/Location</label>
                                                    <div class="form-line">
                                                        <input type="text" id="location" name="location" class="form-control">
                                                    </div>
                                                    <label id="location-error" for="location" class="error"></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Lat</label>
                                                    <div class="form-line">
                                                        <input type="text" id="lat" name="lat" class="form-control">
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Lng</label>
                                                    <div class="form-line">
                                                        <input type="text" id="lng" name="lng" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <br>
                                        <button type="submit" class="btn bg-indigo align-right m-t-15 waves-effect">Submit</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="card">
                            <div class="header">
                                <h2>Information</h2>
                            </div>
                            <div class="body">
                                <ul class="right_info">
                                    <li>Update staff profile information.</li>
                                    <li>Admin user is permitted to all modules.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
@stop