@section('title','FAQ')
@extends('layouts.adminlayout')
@section('content')


<div class="wrapper">
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Dashboard</h1>
                    </div>
                    <div class="col-sm-6">
                        <a href="" data-toggle="modal" data-target="#edit-faq" style="float:right; padding:.8rem; border: 1px solid black; border-radius:6px;">
                            Edit About Us Content
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4>Title: {{$data->title}}</h4>
                                <h3 style="text-align: center;">Content</h3>
                                <?php echo $data->content; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<div id="edit-faq" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit About Us</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>

            </div>
            <div class="modal-body">
                <form id="edit_faq" method="post" action="{{url('/about-us')}}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <label class="col-md-12 col-xs-12 col-sm-12">Title</label>
                            <input type="text" class="form-control" value="{{$data->title}}" id="modal_about_title" name="modal_about_title" />
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <label class="col-md-12 col-xs-12 col-sm-12">Slug</label>
                            <input type="text" class="form-control" value="{{$data->slug}}" id="modal_about_slug" name="modal_about_slug" />
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <label class="col-md-12 col-xs-12 col-sm-12">Content</label>
                            <textarea type="text" class="form-control" id="editor1" name="editor1">{{$data->content}}</textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <input class="btn btn-primary" type="submit" value="submit" />
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>

<script>
    $(document).ready(function() {
        $("#edit_faq").bootstrapValidator({
            framework: 'bootstrap',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            err: {
                container: 'popover'
            },
            fields: {
                modal_about_title: {
                    validators: {
                        notEmpty: {
                            message: "Please enter Title!!"
                        }
                    }
                },
                modal_about_slug: {
                    validators: {
                        notEmpty: {
                            message: "Please enter Slug!!"
                        }
                    }
                },
                editor1:{
                    validators:{
                        notEmpty:{
                            message: "Please enter content!"
                        }
                    }
                }
            }
        });
    });
</script>

@stop