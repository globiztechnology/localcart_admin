@section('title','Store Owners')
@extends('layouts.adminlayout')
@section('content')
<div class="wrapper">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Store Owners</h1>
                    </div>
                </div>
            </div>
        </div>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <form method="post" id="view_shop" action="{{url('/stores')}}">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="select_user">Select Store Owner</label>
                                <select type="text" class="form-control" id="user_id" name="user_id">
                                    <option value="">Choose Store Owner..</option>
                                    @foreach($users as $key => $user)
                                    <option value="{{$user->id}}">{{$user->mobile_no}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <center><input type="submit" class="btn btn-primary" value="view"></center>
                            </div>
                        </form>
                    </div>
                </div>
                @if(isset($shop_details))
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>S.no</th>
                                            <th>Store Name</th>
                                            <th>Address</th>
                                            <th>Store Open Time</th>
                                            <th>Store Close Time</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @include('admin.tables.store_data')
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                @else
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <h2 style="text-align:center;">No Store Found!</h2>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </section>
    </div>
</div>
@stop