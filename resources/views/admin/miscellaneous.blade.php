@section('title','Miscellaneous')
@extends('layouts.adminlayout')
@section('content')
    <div class="wrapper">   
        <div class="content-wrapper">
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark">Miscellaneous</h1>
                        </div>
                    </div>
                </div>
            </div>
            <section class="content">   
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="card">
                                <div class="card-body">
                                    <table class="table">
                                        <thead>
                                            <th>S.No</th>
                                            <th>Key</th>
                                            <th>Value</th>
                                            <th>Actions</th>
                                        </thead>
                                        <tbody>
                                            @foreach($data as $key => $row)
                                                <tr>
                                                    <td>{{++$key}}</td>
                                                    <td>{{$row->text}}</td>  
                                                    <td>{{$row->value}}</td>
                                                    <td>
                                                        <a href="javascript:;" data-toggle="modal" data-target="#modal-edit-option" rel="{{$row->id}}" data-text="{{$row->text}}" data-value="{{$row->value}}" class="edit-option"><span class="fa fa-edit"></span></a>&nbsp;
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    <div id="modal-edit-option" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit Option</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                
            </div>
            <div class="modal-body">
                <form id="edit-category" method="post" action="{{url('/miscellaneous')}}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="modal_edit_option_id" id="modal_edit_option_id" >
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <label class="col-md-12 col-xs-12 col-sm-12">Option Text</label>
                            <input type="text" class="form-control" id="modal_edit_option_text" name="modal_edit_option_text" />
                        </div>
                    </div> 
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <label class="col-md-12 col-xs-12 col-sm-12">Value</label>
                            <input type="text" class="form-control" id="modal_edit_option_value" name="modal_edit_option_value" />
                        </div>
                    </div> 
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <input class="btn btn-primary" type="submit" value="submit" />
                        </div>
                    </div>      
                </form>
            </div>
            <!-- <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div> -->
            </div>

        </div>
    </div>

    <script>
        $(document).ready(function(){
            $('.edit-option').on('click',function(){
                $('#modal_edit_option_id').val($(this).attr('rel'));
                $('#modal_edit_option_text').val($(this).attr('data-text'));
                $('#modal_edit_option_value').val($(this).attr('data-value'));
            });
        });
    </script>    
@stop