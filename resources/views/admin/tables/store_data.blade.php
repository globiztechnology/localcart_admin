@foreach($shop_details as $key =>$shop)
<tr>
    <td>{{++$key}}</td>
    <td>{{$shop['shop_name']}}</td>
    <td>
        {{$shop['address_details']->address}}
        @if(isset($shop['address_details']['state_details']))
        ,{{$shop['address_details']['state_details']['name']}}
        @endif
        @if(isset($shop['address_details']['city_details']['name']))
        ,{{$shop['address_details']['city_details']['name']}}
        @endif
    </td>
    <td>{{$shop['shop_open_time']}}</td>
    <td>{{$shop['shop_close_time']}}</td>
</tr>
@endforeach
<td colspan="5">
    {{$shop_details->links()}}
</td>