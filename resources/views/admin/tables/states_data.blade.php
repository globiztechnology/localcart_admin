@foreach($data as $key => $row)
<tr>
    <td>{{++$key}}</td>
    <td>{{$row->name}}</td>
    <td>{{$row['state_country']->name}}</td>
    <td>
        <!-- <a href="{{url('states/edit').'?key='.base64_encode($row->id)}}">
            <span class="fa fa-edit"></span>
        </a>
        &nbsp;&nbsp; -->
        <a href="javascript:;" class="delete-item" data-key="state" rel="{{$row->id}}">
            <span class="fa fa-trash"></span>
        </a>

    </td>
</tr>
@endforeach
<tr>
    <td colspan="4" align="center">
        {{$data->links()}}
    </td>
</tr>