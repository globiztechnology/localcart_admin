@foreach($orders as $row)
<tr>
    <td>{{$row['customer_details']['name']}}</td>
    <td>{{$row['customer_details']['mobile_no']}}</td>
    <td>{{$row['shop_details']['shop_name']}}</td>
    <td>{{$row['shop_details']['shopkeeper_details']['name']}}</td>
    <td>{{$row['shop_details']['shopkeeper_details']['mobile_no']}}</td>
    @if($row->notes)
    <td>{{$row->notes}}</td>
    @else
    <td>-</td>
    @endif
    <td>{{$row->pickup_time}}</td>
    <td>{{$row->status}}</td>
    <td>
        <a href="{{url('/view-order/'.base64_encode($row->id))}}">
            <span class="fa fa-eye"></span>
        </a>&nbsp;
        <a href="javascript:;" class="edit-order" data-toggle="modal" data-target="#modal-edit-orders" rel="{{$row->id}}" data-status="{{$row->status}}">
            <span class="fa fa-edit"></span>
        </a>&nbsp;
        <a href="javascript:;" class="delete-order" rel="{{$row->id}}">
            <span class="fa fa-trash"></span>
        </a>

    </td>
</tr>
@endforeach
<tr>
    <td colspan="8">
        {{ $orders->links() }}
    </td>
</tr>