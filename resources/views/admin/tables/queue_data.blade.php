@foreach($store_queue as $key =>$shop)
<tr>
    <td>{{++$key}}</td>
    <td>{{$shop['customer_details']->first_name}} {{$shop['customer_details']->last_name}}</td>
    <td>{{$shop['shop_details']->shop_name}}</td>
    <td>
        @if($shop['status']=='1')
        Accepted
        @elseif($shop['status']=='2')
        Rejected
        @else
        <?php echo "Pending"; ?>&nbsp;&nbsp;&nbsp;
        <a data-shopid="{{$shop->shop_id}}" data-userid="{{$shop->user_id}}" data-fromuid="{{$shop['shop_details']->user_id}}" rel="{{$shop->id}}" href="javascript:;" data-toggle="modal" data-target="#modal-edit-status" class="edit-status">
            <span class="fa fa-edit"></span>
        </a>
        @endif
    </td>
    <td>
        @if($shop['checkout']=='1')
        Checked Out
        @else
        Pending
        @endif
    </td>
    <td>
        @if($shop['action_date'])
        {{date('Y-m-d h:i:s',$shop['action_date'])}}
        @else
        -
        @endif
    </td>

</tr>
@endforeach
<tr>
    <td colspan="6">
        {{$store_queue->links()}}
    </td>
</tr>