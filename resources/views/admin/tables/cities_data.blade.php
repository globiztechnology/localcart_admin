@foreach($data as $key => $row)
    <tr>
        <td>{{++$key}}</td>
        <td>{{$row->name}}</td>
        <td>{{$row['city_state']->name}}</td>
        <td>{{$row['city_state']['state_country']->name}}</td>
        <td>
            <a href="javascript:;" class="delete-item" data-key="state" rel="{{$row->id}}">
                <span class="fa fa-trash"></span>
            </a>
        </td>
    </tr>
@endforeach
<tr>
    <td colspan="5" align="center">
        {{$data->links()}}
    </td>
</tr>