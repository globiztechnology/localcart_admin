    @foreach($users as $key => $row)
    <tr>
        <td>{{$row->name}}</td>
        <td>{{$row->email}}</td>
        <td>{{$row->mobile_no}}</td>
        <td>
            <label class="switch">
                <input class="toggle-btn" rel="{{$row->id}}" type="checkbox" <?php
                                                                                if ($row->status) {
                                                                                    echo "checked";
                                                                                }
                                                                                ?>>
                <span class="slider round"></span>
            </label>
        </td>
        <td>
            <a href="{{url('/customer/view').'/'.base64_encode($row->id)}}">
                <span class="fa fa-edit"></span>
            </a>&nbsp;&nbsp;
            <a href="javascript:;" class="delete-user" data-type="customer" rel="{{$row->id}}">
                <span class="fa fa-trash"></span>
            </a>
        </td>
    </tr>
    @endforeach
    <tr>
        <td class="paging-users" colspan="6" align="center">
            {{ $users->links() }}
        </td>
    </tr>