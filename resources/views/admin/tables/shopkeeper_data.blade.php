@foreach($users as $key => $row)
<tr>
    <td>{{++$key}}</td>
    <td>{{$row->name}}</td>
    <td>{{$row->email}}</td>
    <td>{{$row->mobile_no}}</td>
    <td>
        @if(isset($row['address_details']->address)&&isset($row['address_details']['city_details']->name)&&isset($row['address_details']['state_details']->name))
        {{$row['address_details']->address.', '.$row['address_details']['city_details']->name.', '.$row['address_details']['state_details']->name}}
        @else
        -
        @endif
    </td>
    <td>
        <label class="switch">
            <input class="toggle-btn" rel="{{$row->id}}" type="checkbox" <?php
                                                                            if ($row->status) {
                                                                                echo "checked";
                                                                            }
                                                                            ?>>
            <span class="slider round"></span>
        </label>
    </td>
    <td>
        <a href="{{url('/stores/'.base64_encode($row->id))}}">
            <span class="fa fa-eye"></span>
        </a>
        <a href="javascript:;" class="delete-user" data-type="shopkeeper" rel="{{$row->id}}">
            <span class="fa fa-trash"></span>
        </a>
    </td>
</tr>
@endforeach
<tr>
    <td class="paging-users" colspan="7" align="center">
        {{ $users->links() }}
    </td>
</tr>