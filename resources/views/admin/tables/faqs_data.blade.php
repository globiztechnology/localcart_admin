@foreach($data as $key => $row)
    <tr>
        <td>{{++$key}}</td>
        <td>{{$row->question}}</td>
        <td>{{$row->answer}}</td>
        <td>
            <a href="javascript:;" data-toggle="modal" data-target="#modal-edit-faq" class="convert-faq-to-modal" rel="{{$row->id}}" data-question="{{$row->question}}" data-answer="{{$row->answer}}">
                <span class="fa fa-edit"></span>
            </a>
            <a href="javascript:;" class="delete-faq" rel="{{$row->id}}">
                <span class="fa fa-trash"></span>
            </a>
        </td>
    </tr>
@endforeach
