@section('title','Stores')
@extends('layouts.adminlayout')
@section('content')
<div class="wrapper">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h4 class="m-0 text-dark"><a href="{{url('/store-owners')}}">Store-Owners</a> > Stores</h4>
                    </div>
                </div>
            </div>
        </div>
        <section class="content">
            <div class="container-fluid">
                @if(count($shops))
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <table class="table">
                                    <thead>
                                        <th>S.No.</th>
                                        <th>Shop Name</th>
                                        <th>Category</th>
                                        <th>Shop Opening Time</th>
                                        <th>Shop Closing Time</th>
                                        <th>Status</th>
                                    </thead>
                                    <tbody>
                                        @foreach($shops as $key => $shop)
                                        <td>{{++$key}}</td>
                                        <td>{{$shop->shop_name}}</td>
                                        <td>{{$shop['category_details']->category_name}}</td>
                                        <td>{{$shop->shop_open_time}}</td>
                                        <td>{{$shop->shop_close_time}}</td>
                                        <td>
                                            <label class="switch">
                                                <input class="toggle-btn" rel="{{$shop->id}}" type="checkbox" <?php
                                                                                                                if ($shop->status) {
                                                                                                                    echo "checked";
                                                                                                                }
                                                                                                                ?>>
                                                <span class="slider round"></span>
                                            </label>
                                        </td>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                @else
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4>No store found, associating to this store-owner!</h4>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </section>
    </div>
</div>
@stop