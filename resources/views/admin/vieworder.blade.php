@section('title','Order Details')
@extends('layouts.adminlayout')
@section('content')
<div class="wrapper">
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark"><a href="{{url('/orders')}}">Orders</a> > Orders Details</h1>
                    </div>
                </div>
            </div>
        </div>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="card">
                            <div class="card-body">
                                <h4>Customer Name:</h4>
                                <p>{{$order_details['customer_details']->name}}</p>
                                <h4>Mobile No.</h4>
                                <p>{{$order_details['customer_details']->mobile_no}}</p>
                                <h5>Customer Comments</h5>
                                @if(isset($order_details->notes))
                                    <p>{{$order_details->notes}}</p>
                                @else
                                    <p>-</p>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6"></div>
                    <div class="col-sm-3">
                        <div class="card">
                            <div class="card-body">
                                <h4>Shop Name:</h4>
                                <p>{{$order_details['shop_details']->shop_name}}</p>
                                <h4>Mobile No.</h4>
                                <p>{{$order_details['shop_details']['shopkeeper_details']->mobile_no}}</p>
                                <h5>Store Owner Comment</h5>
                                @if(isset($order_details->store_owner_note))
                                    <p>{{$order_details->store_owner_note}}</p>
                                @else
                                    <p>-</p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <table class="table">
                                    <thead>
                                        <th>S.No.</th>
                                        <th>Item Name</th>
                                        <th>Weight</th>
                                        <th>Quantity</th>
                                        <th>Item Comment</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </thead>
                                    <tbody>
                                        @foreach($order_details['order_item'] as $key => $row)
                                        <tr>
                                            <td>{{++$key}}</td>
                                            <td>{{$row->item_name}}</td>
                                            <td>{{$row->weight}}</td>
                                            <td>{{$row->quantity}}</td>
                                            <td>{{$row->comment}}</td>
                                            <td>
                                                @if($row->status)
                                                In Stock
                                                @else
                                                Out Of Stock
                                                @endif
                                            </td>
                                            <td>
                                                <a href="javascript:;" class="edit-order" data-toggle="modal" data-target="#modal-edit-orders" rel="{{$row->id}}" data-status="{{$row->status}}">
                                                    <span class="fa fa-edit"></span>
                                                </a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<div id="modal-edit-orders" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Change Order Item Status</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>

            </div>
            <div class="modal-body">
                <form id="edit-category" method="post" action="{{url('/edit-order-item-status')}}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="modal_edit_item_id" id="modal_edit_item_id">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <label class="col-md-12 col-xs-12 col-sm-12">Stock Status</label>
                            <!-- <input type="text" class="form-control" id="modal_edit_order_status" name="modal_edit_order_status" /> -->
                            <select class="form-control" id="modal_edit_item_status" name="modal_edit_item_status">

                                <option value="1">In Stock</option>
                                <option value="0">Out of Stock</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <input class="btn btn-primary" type="submit" value="submit" />
                        </div>
                    </div>
                </form>
            </div>
            <!-- <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div> -->
        </div>

    </div>
</div>

<script>
    $(document).ready(function() {
        $('.edit-order').on('click', function() {
            $('#modal_edit_item_status').val($(this).attr('data-status'));
            $('#modal_edit_item_id').val($(this).attr('rel'));
        });
    });
</script>
@stop