@section('title','Customers')
@extends('layouts.adminlayout')
@section('content')
<style>
    table thead tr th {
        padding: .5rm;
    }

    
</style>
<div class="ajax-loader">
    <img src="../images/loader1.gif" />
</div>
<div class="wrapper">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Customers</h1>
                    </div>
                    
                </div>
            </div>
        </div>
        <section class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-xl-12">
                        <div class="input-group">
                            <input class="form-control" id="search-user" style="float:right;" type="text" placeholder="Search Users">
                            <span class="input-group-btn">
                            </span>
                        </div>
                    </div>
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="sorting" data-column-name="name" data-sorting-type="asc" style="cursor:pointer;">
                                                    Name<span id="name_icon"></span>
                                                </th>
                                                <th class="sorting" style="cursor:pointer;" data-column-name="email" data-sorting-type="asc">
                                                    Email<span id="email_icon"></span>
                                                </th>
                                                <th class="sorting" style="cursor:pointer;" data-column-name="mobile_no" data-sorting-type="asc">
                                                    Phone Number<span id="mobile_no_icon"></span>
                                                </th>
                                                <th>Status</th>
                                                <th>Actions</th>
                                            </tr>

                                        </thead>
                                        <tbody>
                                            @include('admin.tables.customer_data')
                                        </tbody>
                                        <input type="hidden" name="hidden_page" id="hidden_page" value="1" />
                                        <input type="hidden" name="hidden_column_name" id="hidden_column_name" value="created_at" />
                                        <input type="hidden" name="hidden_sort_type" id="hidden_sort_type" value="asc" />
                                        <input type="hidden" name="hidden_user_type" id="hidden_user_type" value="customer" />
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>


<script>
    $(document).ready(function() {
        $('.toggle-btn').on('change', function() {
            var user_id = $(this).attr('rel');
            var status = $(this).prop('checked');
            swal({
                    title: "Are you sure?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: "update-status",
                            type: "POST",
                            data: {
                                user_id,
                                status
                            },
                            success: function(data) {
                                if (data.status) {
                                    swal(data.message, {
                                        icon: "success",
                                    });
                                } else {
                                    swal(data.message, {
                                        icon: "error"
                                    });
                                    location.reload();
                                }
                            }
                        });

                    } else {
                        swal("Your data is safe!");
                        location.reload();
                    }
                });

        });
    });
</script>




@stop