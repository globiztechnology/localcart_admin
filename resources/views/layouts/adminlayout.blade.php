<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>LocalCart | @yield('title')</title>

    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- ==== Font Awesome ==== -->
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.11.0/css/all.css">
    <!-- ==== Ionicons ==== -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- ==== iCheck ==== -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/icheck-bootstrap@3.0.1/icheck-bootstrap.min.css">
    <!-- ==== Select2 ==== -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/css/select2.min.css">
    <!-- ==== Bootsrap table ==== -->
    <!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css"> -->
    <!-- ==== Theme style ==== -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/admin-lte@3.0.1/dist/css/adminlte.min.css">
    <!-- ==== overlayScrollbars ==== -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/overlayscrollbars/1.10.2/css/OverlayScrollbars.min.css">
    <link rel="stylesheet" href="../assets/css/style.css">
    <!-- ==== Dropzone CSS : Drag Drop Files ==== -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.0/basic.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.0/dropzone.min.css" />

    <link href="https://www.jqueryscript.net/css/jquerysctipttop.css?v3" rel="stylesheet" type="text/css">

    <!-- ==== Google Font: Source Sans Pro ==== -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- ==== Custom css ==== -->

    <link rel="stylesheet" href="https://globizdevserver.com/autopsy/assets/backend/dist/css/BsMultiSelect.css">

    <link rel="stylesheet" href="https://globizdevserver.com/autopsy/assets/backend/dist/css/custom.css">

    <link rel="stylesheet" href="https://globizdevserver.com/autopsy/assets/backend/dist/css/developer.css">

    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <!-- <link rel="stylesheet" href="//cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css"> -->
    <style>
        .brand-image {
            float: left;
            line-height: .8;
            margin-left: 0.8rem;
            padding-left: 10px;
            padding-right: 10px;
            margin-right: 0.5rem;
            margin-top: -3px;
            /* max-height: 69px; */
            width: 88%;
        }

        div .has-error small {
            color: red;
        }
    </style>
    <script>
        var site_url = 'https://globizdevserver.com/autopsy/';
    </script>
    <script>
        var admin_url = 'https://globizdevserver.com/autopsy/admin/';
    </script>
    <!-- ==== jQuery ==== -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!-- ==== jQuery UI ==== -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <!-- ==== Bootstrap ==== -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
    <!-- ==== Select2 ==== -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/js/select2.full.min.js"></script>
    <!-- ==== overlayScrollbars ==== -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/overlayscrollbars/1.10.2/js/jquery.overlayScrollbars.min.js"></script>
    <!-- ==== Bootstap table ==== -->
    <!-- <script src="//cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script> -->
    <!-- <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script> -->
    <!-- ==== ckeditor js ==== -->
    <script src="https://cdn.ckeditor.com/4.13.0/standard/ckeditor.js"></script>
    <!-- ==== AdminLTE App ==== -->
    <script src="https://cdn.jsdelivr.net/npm/admin-lte@3.0.1/dist/js/adminlte.min.js"></script>
    <!-- ==== jquery-validate ==== -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
    <!-- ==== jQuery Ajax Form js ==== -->
    <script src="https://malsup.github.io/min/jquery.form.min.js"></script>
    <!-- ==== Custom ckeditor image ==== -->
    <!-- ==== Dropzone Js : for Drag Drop files ==== -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.0/min/dropzone.min.js"></script>

    <script src="https://globizdevserver.com/autopsy/assets/backend/dist/js/ckeditor_image_plugin.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>


    <script src="https://globizdevserver.com/autopsy/assets/backend/dist/js/BsMultiSelect.js"></script>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.5.3/js/bootstrapValidator.js"></script>

    <script src="{{url('./public/assets/js/select2.min.js')}}" type='text/javascript'></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.2/js/toastr.min.js"></script>
    <script src="https://cdn.ckeditor.com/4.13.0/standard/ckeditor.js"></script>
    <script>
        $(document).ready(function() {
            if ($('#editor1').length) {
                CKEDITOR.replace('editor1');
            }
        });
    </script>
    <!-- ==== Custom js ==== -->
    
    <script>
        @if(Session::has('success'))
        $(function() {
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
            toastr.success("{{ Session::get('success') }}");
        });
        @endif
    </script>
    <script>
        @if(Session::has('error'))
        $(function() {
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
            toastr.error("{{ Session::get('error') }}");
        });
        @endif
    </script>

</head>

<body>
    @include('common.header')
    @include('common.sidebar')
    <section class="main-content">
        <div>
            @yield('content')
        </div>
    </section>
    @include('common.footer')
    
    <script>
        $('select').select2();
    </script>
    
    <script src="../assets/js/custom.js"></script>
    
</body>

</html>