        <!-- ==== Navbar Start ==== -->
        <nav class="main-header navbar navbar-expand navbar-dark">
            <!-- ==== Left navbar links ==== -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/logout')}}"><i class="far fa-sign-out"></i>&nbsp;&nbsp;Logout</a>
                </li>
            </ul>
            <!-- ==== Right navbar links ==== -->
            <ul class="navbar-nav ml-auto">
                <li class="dropdown user_drop">
                    <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="user_img">
                            <img src="{{url('./public/admin_profile/'.Auth::guard('admin')->user()->profile_pic)}}" alt="..." />
                        </span>
                        <span class="user_name">admin</span>
                    </button>
                    <ul class="dropdown-menu">
                        <li>
                            <a class="dropdown-item" href="{{url('/profile')}}">
                                <i class="far fa-user"></i>&nbsp;&nbsp;Profile
                            </a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="{{url('/logout')}}">
                                <i class="far fa-sign-out"></i>&nbsp;&nbsp;Logout
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>

        <!-- ==== Navbar End ==== -->