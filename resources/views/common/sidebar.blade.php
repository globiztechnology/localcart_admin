        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- ==== Brand Logo ==== -->

            <img src="{{url('./public/images/logo.png')}}" alt="Logo" class="brand-image img-circle" style="opacity: .8">


            <!-- ==== Sidebar ==== -->
            <div class="sidebar">
                <!-- ==== Sidebar Menu ==== -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column nav-flat" data-widget="treeview" role="menu" data-accordion="false">
                        <li class="nav-item">
                            <a href="{{url('/dashboard')}}" class="nav-link">
                                <i class="nav-icon fas fa-tachometer-alt"></i>
                                <p>
                                    Dashboard
                                    <i class="right fas fa-angle-right"></i>
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('/customers')}}" class="nav-link">
                                <i class="nav-icon fas fa-user"></i>
                                <p>
                                    Customers
                                    <i class="right fas fa-angle-right"></i>
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('/store-owners')}}" class="nav-link">
                                <i class="nav-icon fas fa-user"></i>
                                <p>
                                    Store Owners
                                    <i class="right fas fa-angle-right"></i>
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('/stores')}}" class="nav-link">
                                <i class="nav-icon fas fa-user"></i>
                                <p>
                                    Stores
                                    <i class="right fas fa-angle-right"></i>
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('/orders')}}" class="nav-link">
                                <i class="nav-icon fas fa-shopping-cart"></i>
                                <p>
                                    Orders
                                    <i class="right fas fa-angle-right"></i>
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('/categories')}}" class="nav-link">
                                <i class="nav-icon fas fa-list-alt"></i>
                                <p>
                                    Shop Categories
                                    <i class="right fas fa-angle-right"></i>
                                </p>
                            </a>
                        </li>
                        <li class="nav-item has-treeview ">
                            <a href="#" class="nav-link ">
                                <i class="nav-icon fal fa-clone"></i>
                                <p>
                                    Content Management
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{url('faq')}}" class="nav-link ">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>FAQ</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{url('terms&conditions')}}" class="nav-link ">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Terms & Conditions</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{url('about-us')}}" class="nav-link ">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>About Us</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{url('privacy-policy')}}" class="nav-link ">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Privacy Policy</p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="nav-item">
                            <a href="{{url('/states')}}" class="nav-link">
                                <i class="nav-icon fas fa-building"></i>
                                <p>
                                    Manage States
                                    <i class="right fas fa-angle-right"></i>
                                </p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{url('/districts')}}" class="nav-link">
                                <i class="nav-icon fas fa-home"></i>
                                <p>
                                    Manage Districts
                                    <i class="right fas fa-angle-right"></i>
                                </p>
                            </a>
                        </li>

                        <!-- <li class="nav-item">
                            <a href="{{url('/miscellaneous')}}" class="nav-link">
                                <i class="nav-icon fas fa-list-alt"></i>
                                <p>
                                    Miscellaneous
                                    <i class="right fas fa-angle-right"></i>
                                </p>
                            </a>
                        </li> -->
                        <!-- <li class="nav-item has-treeview ">
                            <a href="#" class="nav-link ">
                                <i class="nav-icon fal fa-book"></i>
                                <p>
                                    Users
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="#" class="nav-link ">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Customers</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link ">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Shopkeepers</p>
                                    </a>
                                </li>
                            </ul>
                        </li> -->

                        <!-- <li class="nav-item has-treeview ">
                            <a href="#" class="nav-link ">
                                <i class="nav-icon fal fa-clone"></i>
                                <p>
                                    Articles
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="https://globizdevserver.com/autopsy/admin/post" class="nav-link ">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>All Articles</p>
                                    </a>
                                </li>
                                                                <li class="nav-item">
                                    <a href="https://globizdevserver.com/autopsy/admin/post/add?language=en" class="nav-link ">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Add New</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="https://globizdevserver.com/autopsy/admin/post/add_category" class="nav-link ">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Add Category</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="https://globizdevserver.com/autopsy/admin/post/category" class="nav-link ">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>All Category</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="https://globizdevserver.com/autopsy/admin/post/add_tags" class="nav-link ">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Add Tags</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="https://globizdevserver.com/autopsy/admin/post/tags" class="nav-link ">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>All Tags</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item has-treeview ">
                            <a href="#" class="nav-link ">
                                <i class="fal fa-cars nav-icon"></i>
                                <p>
                                    Cars
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="https://globizdevserver.com/autopsy/admin/car_models" class="nav-link ">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Models</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="https://globizdevserver.com/autopsy/admin/car_category" class="nav-link ">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Categories</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="https://globizdevserver.com/autopsy/admin/car_cylinder_capacity" class="nav-link ">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Cylinder Capacity</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="https://globizdevserver.com/autopsy/admin/car_engine_label" class="nav-link ">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Engine lables</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="https://globizdevserver.com/autopsy/admin/car_fuel_type/index" class="nav-link ">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Fuel type</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item has-treeview ">
                            <a href="#" class="nav-link ">
                                <i class="fas fa-motorcycle nav-icon"></i>
                                <p>
                                    Bikes
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="https://globizdevserver.com/autopsy/admin/bike_models" class="nav-link ">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Models</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="https://globizdevserver.com/autopsy/admin/bike_category" class="nav-link ">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Categories</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="https://globizdevserver.com/autopsy/admin/bike_cylinder_capacity" class="nav-link ">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Cylinder Capacity</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="https://globizdevserver.com/autopsy/admin/bike_engine_label" class="nav-link ">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Engine lables</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="https://globizdevserver.com/autopsy/admin/bike_fuel_type/index" class="nav-link ">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Fuel type</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item has-treeview ">
                            <a href="#" class="nav-link ">
                                <i class="nav-icon fal fa-book"></i>
                                <p>
                                    Forensic
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="https://globizdevserver.com/autopsy/admin/forensic" class="nav-link ">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>All Forensic</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="https://globizdevserver.com/autopsy/admin/forensic/add" class="nav-link ">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Add New</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item has-treeview ">
                            <a href="#" class="nav-link ">
                                <i class="fal fa-check-circle nav-icon"></i>
                                <p>
                                    Check Up
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="https://globizdevserver.com/autopsy/admin/check_up" class="nav-link ">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>All Check Up</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="https://globizdevserver.com/autopsy/admin/check_up/add" class="nav-link ">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Add New</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item has-treeview ">
                            <a href="#" class="nav-link ">
                                <i class="fal fa-tools nav-icon"></i>
                                <p>
                                    Tools
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="https://globizdevserver.com/autopsy/admin/tools" class="nav-link ">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>All Tools</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="https://globizdevserver.com/autopsy/admin/tools/add" class="nav-link ">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Add New</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item has-treeview ">
                            <a href="#" class="nav-link ">
                                <i class="fal fa-user-friends nav-icon"></i>
                                <p>
                                    People
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="https://globizdevserver.com/autopsy/admin/people" class="nav-link ">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>All People</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="https://globizdevserver.com/autopsy/admin/people/add" class="nav-link ">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Add New</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item has-treeview ">
                            <a href="#" class="nav-link ">
                                <i class="fal fa-user-tie nav-icon"></i>
                                <p>
                                    Professional Organisation
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="https://globizdevserver.com/autopsy/admin/prof_org" class="nav-link ">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>All Professional Organisation</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="https://globizdevserver.com/autopsy/admin/prof_org/add" class="nav-link ">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Add New</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                                                
                        <li class="nav-item has-treeview ">
                            <a href="#" class="nav-link ">
                                <i class="nav-icon fal fa-users"></i>
                                <p>
                                    Staff Manage
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="https://globizdevserver.com/autopsy/admin/staff" class="nav-link ">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>All Staff</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="https://globizdevserver.com/autopsy/admin/staff/add" class="nav-link ">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Add New</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item has-treeview ">
                            <a href="#" class="nav-link ">
                                <i class="nav-icon fab fa-get-pocket"></i>
                                <p>
                                    FAQ
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="https://globizdevserver.com/autopsy/admin/faq" class="nav-link ">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>All faq</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="https://globizdevserver.com/autopsy/admin/faq/add" class="nav-link ">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Add New</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item has-treeview ">
                            <a href="#" class="nav-link ">
                                <i class="nav-icon far fa-image"></i>
                                <p>
                                    Banners
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="https://globizdevserver.com/autopsy/admin/banners" class="nav-link ">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>All Banners</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="https://globizdevserver.com/autopsy/admin/banners/add" class="nav-link ">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Add New</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item has-treeview ">
                            <a href="#" class="nav-link ">
                                <i class="nav-icon fal fa-envelope-open-text"></i>
                                <p>
                                    Newsletter
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="https://globizdevserver.com/autopsy/admin/newsletters" class="nav-link ">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>All Newsletter</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="https://globizdevserver.com/autopsy/admin/newsletters/add" class="nav-link ">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Add New</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        
                        <li class="nav-item">
                            <a href="https://globizdevserver.com/autopsy/admin/settings" class="nav-link">
                                <i class="nav-icon nav-icon fal fa-cogs"></i>
                                <p>
                                    Settings
                                    <i class="right fas fa-angle-right"></i>
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="https://globizdevserver.com/autopsy/admin/contact_request" class="nav-link">
                                <i class="nav-icon nav-icon fal fa-phone-square-alt"></i>
                                <p>
                                    Contact Request
                                    <i class="right fas fa-angle-right"></i>
                                </p>
                            </a>
                        </li> -->
                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>
        <!-- Content Wrapper. Contains page content -->