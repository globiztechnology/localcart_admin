        <footer class="main-footer">
            <strong>Copyright &copy; 2020 LocalCart.</strong>
            All rights reserved.
            <div class="float-right d-none d-sm-inline-block">
                <b>Powered By :</b> <a href="https://globiztechnology.com/" target="_blank">Globiz Technology Inc.</a>
            </div>
        </footer>