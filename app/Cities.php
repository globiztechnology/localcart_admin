<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Cities extends Authenticatable
{
    use Notifiable;

    // protected $guard = 'admin';

    protected $table = "cities";
    
    protected $fillable = [
        'state_id','name'
    ];

    public function city_state(){
        return $this->hasOne('App\States','id','state_id');
    }

}
