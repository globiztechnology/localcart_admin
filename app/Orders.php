<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Orders extends Authenticatable
{
    use Notifiable;

    // protected $guard = 'admin';

    // public $timestamps = false;

    protected $table = "orders";
    
    protected $fillable = [
        'user_id','shop_id','notes','pickup_time','store_owner_note','status','created_at','updated_at'
    ];

    public function getCreatedAtAttribute($value){
        return strtotime($value);
    }
    
    public function customer_details(){
        return $this->hasOne('App\User','id','user_id');
    }

    public function shop_details(){
        return $this->hasOne('App\ShopDetails','id','shop_id');
    }

    public function order_item(){
        return $this->hasMany('App\OrderItems','order_id','id');
    }


    public static function changeStatus($order_id,$status,$amount=NULL,$notes=NULL){
        if($amount){
            $order = self::where('id',$order_id)->first();
            $order->amount = $amount;
            if($notes)
                $order->store_owner_note = $notes;
            $order->status = $status;
            return $order->save();
        }
        $order = self::where('id',$order_id)->first();
        $order->status = $status;
        return $order->save();
    }

    public static function getOrders($shop_id,$status=NULL,$page=NULL,$limit=NULL){
        if($status)
            $orders = Orders::where('shop_id',$shop_id)->where('status',$status)->limit($limit)->offset(($page - 1) * $limit)->get();
        else
            $orders = Orders::where('shop_id',$shop_id)->where('status','pending')->count();
        return $orders;
    }   

    public static function orderDetails($order_id){
        $orders = self::find($order_id);
        if($orders){
            $orderdetails = self::where('id',$order_id)->with(['order_item','shop_details'=>function($query){
                $query->select('id','user_id','shop_name','shop_open_time','shop_close_time');
            },'shop_details.shopkeeper_details'=>function($query){
                $query->select('id','name','mobile_no');
            },
            'customer_details'=>function($query){
                $query->select('id','name','mobile_no');
            }])->first();
            return $orderdetails;
        }
        else
            return false;
        
    }
    
    
    
    

}
