<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;


    protected $table = "users";

    protected $fillable = [
        'name', 'email', 'password', 'decoded_password', 'aadhar_no', 'gender', 'user_type', 'mobile_no', 'mobile_verification', 'profile_pic', 'status', 'created_at', 'updated_at', 'wallet', 'otp', 'expire_otp'
    ];

    public function shop_details()
    {
        return $this->hasOne('App\ShopDetails', 'user_id');
    }

    public function address_details()
    {
        return $this->hasOne('App\Addresses', 'user_id');
    }

    public function bank_details()
    {
        return $this->hasOne('App\BankDetails', 'user_id');
    }

    public static function getCustomer($id)
    {
        $user = self::where('id', $id)->with('address_details')->first();
        return $user;
    }

    public static function getShopkeeper($id)
    {
        $user = self::where('id', $id)->with(['address_details', 'shop_details', 'bank_details'])->first();
        return $user;
    }

    public function AauthAcessToken()
    {
        return $this->hasMany('\App\OauthAccessToken');
    }
}
