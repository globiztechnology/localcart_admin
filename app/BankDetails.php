<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class BankDetails extends Authenticatable
{
    use Notifiable;

    // protected $guard = 'admin';

    protected $table = "bank_details";
    
    protected $fillable = [
        'user_id','shop_id','bank_name','account_no','created_at','updated_at'
    ];

    protected $hidden = ['password','remember_token'];
}
