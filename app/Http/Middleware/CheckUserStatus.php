<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\User;
use Session;
use Route;
class CheckUserStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //dd('heerre');
        if (Auth::check()) {
            if(Auth::user()->status != '1'){
                $chk=Auth::user()->AauthAcessToken()->delete();
                if($chk){
                    return response()->json(['status'=>false,'message'=>'Un-Authenticated'],403);
                } else{
                    return response()->json(['status'=>false,'message'=>'Something went wrong,Try again later!']);
                }
            }
        }
        return $next($request);
    }
           
   
    
}
