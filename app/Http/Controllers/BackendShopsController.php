<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Hash;
use Session;
use App\User;
use App\ShopDetails;


class BackendShopsController extends Controller
{
    public function storeownerstores($id){
        $user_id = base64_decode($id);
        $shops = ShopDetails::storeDetails($user_id);
        if(count($shops)){
            Session::flash('success','Stores fetched successfully!');
        }
        else{
            Session::flash('error','No store found!');
        }
        return view('admin.storeownerstore',['shops'=>$shops]);
    }

    public function stores(Request $request){
        $users = User::get();
        $shop_details = ShopDetails::storeDetails();
        if($request->isMethod('POST')){
            $shop_details = ShopDetails::storeDetails($request['user_id']);
            if(count($shop_details)){
                Session::flash('success','Stores fetched successfully!');
            }
            else{
                Session::flash('error','No store associated with this store owner!');
            }
            return view('admin.stores',['users'=>$users,'shop_details'=>$shop_details]);
        }
        return view('admin.stores',['users'=>$users,'shop_details'=>$shop_details]);
    }
}
