<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Hash;
use Session;
use App\Queues;
use App\Countries;
use DB;
use App\States;
use App\Cities;
use App\Addresses;

class RegionController extends Controller
{
    public function countries(Request $request)
    {
        $data = Countries::paginate(15);
        if ($request->ajax()) {
            $query = $request->get('query');
            // dd($query);
            $data = Countries::where('name', 'like', '%' . $query . '%')->paginate(15);
            return view('admin.tables.countries_data', compact('data'));
        }
        return view('admin.countries', compact('data'));
    }

    public function states(Request $request)
    {
        $data = States::where('country_id', 101)->with('state_country')->paginate(15);
        if ($request->ajax()) {
            $query = $request->get('query');
            $data = States::where('country_id', 101)->where('name', 'like', '%' . $query . '%')->paginate(15);
            return view('admin.tables.states_data', compact('data'));
        }
        return view('admin.states', compact('data'));
    }

    public function cities(Request $request)
    {
        $data = Cities::with(['city_state' => function ($query) {
            $query->with('state_country');
        }])->paginate(15);
        if ($request->ajax()) {
            $query = $request->get('query');
            $data = Cities::with(['city_state', 'city_state.state_country'])
                ->where('cities.name', 'like', '%' . $query . '%')
                ->whereHas('city_state.state_country', function ($q) {

                    $q->where('id', '=', '101');
                })
                ->orWhereHas('city_state', function ($queryobj) use ($query) {
                    $queryobj->where('name', 'LIKE', '%' . $query . '%');
                })
                ->paginate(15);
            return view('admin.tables.cities_data', compact('data'));
        }
        return view('admin.cities', compact('data'));
    }

    public function deleteItem(Request $request)
    {
        $key = $request->get('key');
        $id = $request->get('id');
        if ($key == "state") {
            $check = Addresses::where('state', $id)->count();
            if ($check) {
                return response()->json(['status' => false, 'message' => "There are active user from this State, you can't delete this state!"]);
            }
            $del = States::where('id', $id)->with('state_cities')->delete();
            if ($del) {
                return response()->json(['status' => true, 'message' => 'State deleted successfully!']);
            } else {
                return response()->json(['status' => false, 'message' => 'Something went wrong, try agian later!']);
            }
        }
        if ($key == "city") {
            $check = Addresses::where('state', $id)->count();
            if ($check) {
                return response()->json(['status' => false, 'message' => "There are active user from this State, you can't delete this state!"]);
            }
            $del = States::where('id', $id)->with('state_cities')->delete();
            if ($del) {
                return response()->json(['status' => true, 'message' => 'District deleted successfully!']);
            } else {
                return response()->json(['status' => false, 'message' => 'Something went wrong, try agian later!']);
            }
        }
    }

    public function countriesEdit(Request $request)
    {
        $key = base64_decode($request->get('key'));
        $data = Countries::where('id', $key)->first();
        if ($request->isMethod('post')) {
            $country = Countries::find($request['id']);
            $country->timestamps = false;
            $country->name = $request['country'];
            $upd = $country->save();

            if ($upd)
                Session::flash('true', 'Updated successfully!');
            else
                Session::flash('false', 'Something went wrong,try again later!');
            return redirect()->back();
        }
        return view('admin.edit_country', compact('data'));
    }

    public function statesEdit(Request $request)
    {
        $key = base64_decode($request->get('key'));
        $countries = Countries::get();
        $data = States::where('id', $key)->first();
        if ($request->isMethod('post')) {
            $country = States::find($request['id']);
            $country->timestamps = false;
            $country->name = $request['country'];
            $upd = $country->save();

            if ($upd)
                Session::flash('true', 'Updated successfully!');
            else
                Session::flash('false', 'Something went wrong,try again later!');
            return redirect()->back();
        }
        return view('admin.edit_state', compact('data', 'countries'));
    }
}
