<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Hash;
use File;
use App\User;
use App\ShopDetails;
use App\PasswordReset;
use App\Libraries\FileUpload;
use App\Libraries\LocalCart;
use App\BankDetails;
use App\Addresses;
use App\Orders;
use App\OrderItems;

class ShopkeeperController extends Controller
{
    public function shopregister(Request $request){
        if($request['user_type']=='shopkeeper'){
            $validator  = Validator::make($request->all(),[
                'name'=>'required',
                'email'=>'required|email',
                'password'=>'required',
                'mobile_no'=>'required',
                'address'=>'required',
                'category_id'=>'required',
                'gender'=>'required',
                'district'=>'required',
                'state'=>'required',
                'user_type'=>'required',
                'latitude'=>'required',
                'longitude'=>'required',
                'bank_name'=>'required',
                'account_no'=>'required',
            ]);
            if($validator->fails()){
                return response()->json(['status'=>0,'message'=>$validator->errors()]);
            }
            $check = User::where('email',$request['email'])->first();
            if($check){
                return response()->json(['status'=>0,'message'=>'Shopkeeper is already registered with this email!']);
            }
            else{
                $check2 = User::where('mobile_no',$request['mobile_no'])->first();
                if($check2){
                    return response()->json(['status'=>0,'message'=>'User is already registered with this mobile number!']);
                }
                else{
                    $input['name']  = $request['name'];
                    $input['email'] = $request['email'];
                    $input['password']    = Hash::make($request['password']);
                    $input['decoded_password'] = base64_encode($request['password']);
                    $input['mobile_no'] = $request['mobile_no'];
                    $input['gender'] = $request['gender'];
                    if(isset($request['aadhar_no']))
                        $input['aadhar_no'] = $request['aadhar_no'];
                    $input['user_type'] = $request['user_type'];
                    if(isset($request['profile_pic']) && $request['profile_pic']!=NULL){
                        $img = $this->fileUpload($request['profile_pic'],'user_profile');
                        if($img){
                            $input['profile_pic'] = $img;
                        }
                    }
                    $localcart = new LocalCart();
                    $input['otp'] = $localcart->generateOtp();
                    $input['expire_otp'] = now();
                    $input['status'] = '1';
                    $user  =  User::create($input);
                    
                    
                    //adding address
                    $input_add['user_id'] = $user->id;
                    if(isset($request['type']))
                        $input_add['type'] = $request['type'];
                    $input_add['address'] = $request['address'];
                    $input_add['district'] = $request['district'];
                    $input_add['state'] = $request['state'];
                    $input_add['latitude'] = $request['latitude'];
                    $input_add['longitude'] = $request['longitude'];
                    $address = Addresses::create($input_add);


                    //shop details
                    $input_shop['shop_name'] = $request['shop_name'];
                    $input_shop['category_id'] = $request['category_id'];
                    $input_shop['user_id'] = $user->id;
                    $input_shop['shop_open_time'] = $request['shop_open_time'];
                    $input_shop['shop_close_time'] = $request['shop_close_time'];
                    $shop = ShopDetails::create($input_shop);

                    //bank details
                    $input_bank['user_id'] = $user->id;
                    $input_bank['shop_id'] = $shop->id;
                    $input_bank['bank_name'] = $request['bank_name'];
                    $input_bank['account_no'] = $request['account_no'];
                    $bank = Bankdetails::create($input_bank);

                    $success['user_details'] = $user;
                    $success['address_details'] = $address;
                    $success['shop_details'] = $shop;
                    $success['bank_details'] = $bank;
                    $success['user_details']['token'] = $user->createToken('MyApp')-> accessToken;
                    
                    if($user)
                        return response()->json(['status'=>1,'success'=>'Congratulations, You are registered successfully.','data'=>$success]);  
                    else    
                        return response()->json(['status'=>0,'message'=>'Something went wrong, try again later!']);
                }
                
            }
        }
    }
    public function fileUpload($pic,$t){
        $file = new FileUpload();
        $type = $t;
        $img_name = $file->handle_base64_image_upload($pic,$type);
        
        if($img_name)
            return $img_name;
        else
            return false;
        
    }
}
