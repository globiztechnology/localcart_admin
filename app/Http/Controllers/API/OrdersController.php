<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\ShopDetails;
use Carbon\Carbon;
use App\Orders;
use App\OrderItems;
use App\Libraries\Notification;
use App\Libraries\LocalCart;


class OrdersController extends Controller
{

    public function createOrder(Request $request)
    {
        $localcart = new LocalCart();
        if ($localcart->check_minimum_balance(Auth::user()->wallet)) {
            $validator  = Validator::make($request->all(), [
                'shop_id' => 'required',
                'items' => 'required',
                'pickup_time' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 0, 'message' => $validator->errors()]);
            }
            if ($localcart->checkShopStatus($request['shop_id'])) {
                $items = $request['items'];
                $order['shop_id'] = $request['shop_id'];
                $order['user_id'] = Auth::user()->id;
                if (isset($request['notes']))
                    $order['notes'] = $request['notes'];
                $order['pickup_time'] = $request['pickup_time'];
                $ins = Orders::create($order);
                if ($ins) {
                    foreach ($items as $key => $row) {
                        $item['order_id'] = $ins->id;
                        $item['item_name'] = $row['item_name'];
                        if (isset($row['weight'])) {
                            $item['weight'] = $row['weight'];
                            $item['weight_form'] = $row['weight_form'];
                        }
                        if (isset($row['quantity']))
                            $item['quantity'] = $row['quantity'];
                        if (isset($row['comment']))
                            $item['comment'] = $row['comment'];
                        OrderItems::create($item);
                        $item = NULL;
                    }
                    // $notify = new Notification();
                    // $store= ShopDetails::where('id',$request['shop_id'])->first();
                    // $notify->insert_notification($store->user_id,Auth::user()->id,$ins->id,$request['shop_id'],0,'Order has been placed successfully!');
                    return response()->json(['status' => true, 'message' => 'Congratulations, Your order has been placed successfully!']);
                } else {
                    return response()->json(['status' => true, 'message' => 'Something went wrong, try again!']);
                }
            } else {
                return response()->json(['status' => false, 'message' => 'Sorry the store is close at this time, please try after sometime!']);
            }
        }
        return response()->json(['status' => false, 'message' => 'Wallet amount should be greater then minimum amount!']);
    }

    public function myOrders(Request $request)
    {
        if (Auth::user()) {
            $page = $request->has('page') ? $request->get('page') : 1;
            $limit = $request->has('limit') ? $request->get('limit') : 20;
            $data = Orders::where('user_id', Auth::user()->id)->limit($limit)->offset(($page - 1) * $limit)->get();
            if (count($data))
                return response()->json(['status' => true, 'message' => 'Orders fetched successfully!', 'data' => $data]);
            else
                return response()->json(['status' => false, 'message' => 'No order found!']);
        } else
            return response()->json(['status' => false, 'message' => 'Unauthorised!']);
    }

    public function orderDetails(Request $request)
    {
        $validator  = Validator::make($request->all(), [
            'order_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        }
        $data = Orders::orderDetails($request['order_id']);
        if ($data) {
            foreach ($data['order_item'] as $item)
                $item['checked'] = false;
            if ($data)
                return response()->json(['status' => true, 'message' => 'Order details fetched successfully!', 'data' => $data]);
            else
                return response()->json(['status' => false, 'message' => 'Something went wrong,try again later!']);
        }
        else{
            return response()->json(['status'=>false,'message'=>'No order found!']);
        }
    }

    public function allOrder(Request $request)
    {
        $page = $request->has('page') ? $request->get('page') : 1;
        $limit = $request->has('limit') ? $request->get('limit') : 10;
        $d = Orders::limit($limit)->offset(($page - 1) * $limit)->get();
        return response()->json(['status' => true, 'data' => $d]);
    }

    public function changeOrderStatus(Request $request)
    {
        $validator  = Validator::make($request->all(), [
            'order_id' => 'required',
            'status' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        }
        $data = $request->all();
        if (isset($data['amount']))
            $upd = Orders::changeStatus($data['order_id'], $data['status'], $data['amount'], $data['store_owner_note']);
        else
            $upd = Orders::changeStatus($data['order_id'], $data['status']);
        if ($upd) {
            return response()->json(['status' => 1, 'message' => 'Order updated successfully!']);
        } else {
            return response()->json(['status' => 0, 'message' => 'Something went wrong, try again later!']);
        }
    }

    public function changeMultipleOrderStatus(Request $request)
    {
        $validator  = Validator::make($request->all(), [
            'orders' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        }
        $orders = $request['orders'];
        foreach ($orders as $order) {
            Orders::changeStatus($order['order_id'], $order['status']);
        }
        return response()->json(['status' => true, 'message' => 'Orders status updated successfully!']);
    }

    public function shopOrders(Request $request)
    {
        $status = $request->has('status') ? $request->get('status') : false;
        if (!$status)
            return response()->json(['status' => false, 'status is required!']);
        $shop = ShopDetails::where('user_id', Auth::user()->id)->first();
        if ($shop) {
            $page = $request->has('page') ? $request->get('page') : 1;
            $limit = $request->has('limit') ? $request->get('limit') : 20;
            $orders = Orders::getOrders($shop->id, $status, $page, $limit);
            foreach ($orders as $order) {
                $order['checked'] = false;
            }
            if (count($orders))
                return response()->json(['status' => true, 'message' => 'Orders fetched successfully!', 'data' => $orders]);
            else
                return response()->json(['status' => false, 'message' => 'No order found!']);
        }
        return response()->json(['status' => true, 'message' => 'No store associated with this store owner!']);
    }

    public function newOrderCount(Request $request)
    {
        $shop = ShopDetails::where('user_id', Auth::user()->id)->first();
        if ($shop) {
            $cnt = Orders::getOrders($shop->id);
            $order['count'] = $cnt;
            if ($cnt)
                return response()->json(['status' => true, 'message' => 'Orders count fetched successfully!', 'data' => $order]);
            else
                return response()->json(['status' => false, 'message' => 'No order found!']);
        }
        return response()->json(['status' => true, 'message' => 'No store associated with this store owner!']);
    }
}
