<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\ShopDetails;
use App\Category;

use App\Addresses;

class ShopsController extends Controller
{
    public function getCategories()
    {
        $data = Category::select('id', 'category_name')->get();
        if (count($data)) {
            return response()->json(['status' => 1, 'message' => 'Categories fetched successfully!', 'data' => $data]);
        } else {
            return response()->json(['status' => 0, 'message' => 'No category found!']);
        }
    }

    /*
    public function shopregister(Request $request){
        if($request['user_type']=='shopkeeper'){
            $validator  = Validator::make($request->all(),[
                'name'=>'required',
                'email'=>'required|email',
                'password'=>'required',
                'mobile_no'=>'required',
                'address'=>'required',
                'category_id'=>'required',
                'gender'=>'required',
                'district'=>'required',
                'state'=>'required',
                'user_type'=>'required',
                'latitude'=>'required',
                'longitude'=>'required',
            ]);
            if($validator->fails()){
                return response()->json(['status'=>0,'message'=>$validator->errors()]);
            }
            $check = User::where('email',$request['email'])->first();
            if($check){
                return response()->json(['status'=>1,'message'=>'Shopkeeper is already registered with this email!']);
            }
            else{
                $input['name']  = $request['name'];
                $input['email'] = $request['email'];
                $input['password']    = Hash::make($request['password']);
                $input['mobile_no'] = $request['mobile_no'];
                $input['gender'] = $request['gender'];
                if(isset($request['aadhar_no']))
                    $input['aadhar_no'] = $request['aadhar_no'];
               $input['user_type'] = $request['user_type'];
                if(isset($request['profile_pic'])){
                    $input['profile_pic'] = $request['profile_pic'];
                }
                $input['status'] = '1';
                $user  =  User::create($input);
                
                
                //adding address
                $input_add['user_id'] = $user->id;
                if(isset($request['type']))
                    $input_add['type'] = $request['type'];
                $input_add['address'] = $request['address'];
                $input_add['district'] = $request['district'];
                $input_add['state'] = $request['state'];
                $input_add['latitude'] = $request['latitude'];
                $input_add['longitude'] = $request['longitude'];
                $address = Addresses::create($input_add);


                //shop details
                $input_shop['shop_name'] = $request['shop_name'];
                $input_shop['category_id'] = $request['category_id'];
                $input_shop['user_id'] = $user->id;
                $input_shop['shop_open_time'] = $request['shop_open_time'];
                $input_shop['shop_close_time'] = $request['shop_close_time'];
                $shop = ShopDetails::create($input_shop);

                $success['user_details'] = $user;
                $success['address_details'] = $address;
                $success['shop_details'] = $shop;
                $success['user_details']['token'] = $user->createToken('MyApp')-> accessToken;
                
                if($user)
                    return response()->json(['status'=>1,'success'=>'Congratulations, You are registered successfully.','data'=>$success]);  
                else    
                    return response()->json(['status'=>0,'message'=>'Something went wrong, try again later!']);
            }
        }
    }
    */

    public function catgoriesShop(Request $request)
    {

        $validator  = Validator::make($request->all(), [
            'category_id' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()]);
        }
        $this->updateLatLng(Auth::user()->id, $request['latitude'], $request['longitude']);
        $shops = ShopDetails::select('id', 'user_id')
            ->with(['address_details' => function ($query) {
                $query->select('id', 'user_id', 'latitude', 'longitude');
            }])
            ->where('category_id', $request['category_id'])->where('status',1)->get();
        $a = NULL;
        foreach ($shops as $shop) {
            $r = $this->distance($shop['address_details']->latitude, $shop['address_details']->longitude, $request['latitude'], $request['longitude'], 'K');
            if ($r < 5) {
                $a[] = $shop['id'];
            }
        }
        if (isset($a)) {
            $shops = ShopDetails::select('id', 'user_id', 'shop_name', 'shop_open_time', 'shop_close_time')->whereIn('id', $a)->get();
            if ($shops) {
                return response()->json(['status' => 1, 'message' => 'Shops found successfully!', 'data' => $shops]);
            }
        } else {
            return response()->json(['status' => 0, 'message' => 'No shop found,with this category & near by you!']);
        }
    }

    public function distance($lat1, $lon1, $lat2, $lon2, $unit)
    {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        // dd($miles);
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }

    public function shop_details(Request $request)
    {
        $validator  = Validator::make($request->all(), [
            'shop_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        }
        $shops = ShopDetails::with(['address_details', 'shopkeeper_details' => function ($query) {
                $query->select('id', 'name', 'email', 'mobile_no');
            }])
            ->where('id', $request['shop_id'])->first();
        if ($shops)
            return response()->json(['status' => 1, 'message' => 'Shops fetched successfully!', 'data' => $shops]);
        else
            return response()->json(['status' => 0, 'message', 'No shop found!']);
    }

    public function updateLatLng($id, $lat, $lng)
    {
        $upd = Addresses::where('user_id', $id)->update(['latitude' => $lat, 'longitude' => $lng]);
        if ($upd)
            return true;
        else
            return false;
    }

    public function updateShopStatus(Request $request){
        $validator  = Validator::make($request->all(), [
            'shop_id' => 'required',
            'status' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        }
        $shop_id = $request['shop_id'];
        $status = $request['status'];
        $upd = ShopDetails::updateStatus($shop_id,$status);
        if($upd){
            $shop = ShopDetails::singleStoreDetail($shop_id);
            return response()->json(['status'=>true,'message'=>'Updated successfully!','data'=>$shop]);
        }
        else
            return response()->json(['status'=>false,'message'=>'Something went wrong, try again later!']);

    }
}
