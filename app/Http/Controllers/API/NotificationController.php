<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Hash;
use File;
use App\User;
use App\ShopDetails;
use App\Notifications;
use Carbon\Carbon;


class NotificationController extends Controller
{
    public function notificationList(){
        $list = Notifications::
            where('to_user_id',Auth::user()->id)
            ->where('read',0)
            ->get();
        if(count($list)){
            Notifications::where('to_user_id',Auth::user()->id)->update(['read'=>1]);
            return response()->json(['status'=>true,'message'=>'Notification fetched successfully!','data'=>$list]);
        }
        else
            return response()->json(['status'=>false,'message'=>'No notification found!']);
    }

    public function notificationCount(){
        $notify_count = Notifications::where('to_user_id',Auth::user()->id)->count();
        $a['count'] = $notify_count;
        if($notify_count)
            return response()->json(['status'=>true,'message'=>'Notification count fetched successfully!','data'=>$a]);
        else
            return response()->json(['status'=>false,'message'=>'No notification found!']);
    }

}