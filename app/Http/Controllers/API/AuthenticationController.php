<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\ShopDetails;
use App\PasswordReset;
use App\Category;
use Carbon\Carbon;


class AuthenticationController extends Controller
{
    public function login(Request $request){ 
        $validator  = Validator::make($request->all(),[
            'mobile_no'=>'required',
            'password'=>'required',
        ]);
        if($validator->fails()){
            return response()->json(['status'=>0,'message'=>$validator->errors()->first()]);
        }
        if(Auth::attempt(['mobile_no' => $request['mobile_no'], 'password' => $request['password']])){
            if(Auth::user()->status=='0'){
                return response()->json(['status'=>false,'message'=>'Un-Authenicated!'],403);
            }
            if(Auth::user()->user_type=="customer") 
                $user = User::getCustomer(Auth::user()->id); 
            else
                $user = User::getShopkeeper(Auth::user()->id);

            $user['token'] = Auth::user()->createToken('MyApp')-> accessToken;  
            return response()->json(['status'=>1,'message'=>'logged in successfully!','data'=>$user]); 
        } 
        else{ 
            return response()->json(['status'=>false,'message'=>'Wrong mobile number or password!']); 
        } 
    }

    public function mobileVerification(Request $request){
        $validator  = Validator::make($request->all(), [
            'otp' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        }
        $data = $request->all();

        $otp_verifed = User::where('id',Auth::user()->id)->where('otp', $data['otp'])->first();
        if (!$otp_verifed)
            return response()->json(['status' => false, 'message' => 'Otp is invalid!']);

        if (Carbon::parse($otp_verifed->expire_otp)->addMinutes(10)->isPast()) {
            User::where('id',Auth::user()->id)->update(['otp'=>NULL,'expire_otp'=>NULL]);
            return response()->json([
                'status' => false,
                'message' => 'The otp is expired, please try again!'
            ]);
        }
        $upd = User::where('id',Auth::user()->id)->update(['otp'=>NULL,'expire_otp'=>NULL,'mobile_verification'=>1]);

        if($upd){
            $user = User::where('id',Auth::user()->id)->first();
            return response()->json(['status' => true, 'message' => 'Otp verified successfully!', 'data' => $user]);
        }
        else{
            return response()->json(['status'=>false,'message'=>'Something went wrong,try again later!']);
        }
        
    }
}