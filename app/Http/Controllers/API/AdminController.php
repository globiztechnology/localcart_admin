<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Hash;
use File;
use App\User;
use App\ShopDetails;
use App\PasswordReset;
use App\Category;
use Carbon\Carbon;
use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;
use App\BankDetails;
use App\Addresses;
use App\Orders;
use App\OrderItems;
use App\Libraries\LocalCart;
use App\Libraries\FileUpload;
use App\States;
use App\Cities;
use App\Contents;
use App\Faqs;
use App\Libraries\SendSMS;

class AdminController extends Controller
{

    public function imageupload(Request $request){

        $file = new FileUpload();
        $type = $request['type'];
        $img_name = $file->handle_base64_image_upload($request['image'],$type);
        $path =  'http://globizcloudserver.com/localcart/public/'.$type;
        $img_url = $path.'/'.$img_name;
        $image['image_name'] = $img_name;
        $image['url'] = $img_url;
        if($img_name){
            $upd = User::where('id', Auth::user()->id)->update(['profile_pic' => $img_name]);
            if ($upd) {
                if(Auth::user()->user_type=="customer") 
                    $user = User::getCustomer(Auth::user()->id); 
                else
                    $user = User::getShopkeeper(Auth::user()->id);
                $user['image_name'] = $img_name;
                $user['url'] = $img_url;
                return response()->json(['status' => true, 'message' => 'Image uploaded successfully', 'data' => $user]);
            } else {
                return response()->json(['status' => true, 'message' => 'Something went wrong, try again later!']);
            }
        }
        else
            return response()->json(['status'=>false,'message'=>'Something went wrong, try again later!']);

        
        /*if(is_uploaded_file(base64_decode($request['image']))){
            $type = $request['type'];
            $file = base64_decode($request['image']);
            // return response()->json(['status'=>1,'message'=>'received image in base 64!']);
            $img_name = date('Y_m_d_H:i:s').'_'.$file->getClientOriginalName();
            $file->move('./public/'.$type.'/',$img_name);
            $path = 'http://globizcloudserver.com/localcart/public/'.$type;
            $img_url = $path.'/'.$img_name;
            $img['img_name'] = $img_name;
            $img['img_url'] = $img_url;
            if($img_url)
                return response()->json(['status'=>true,'message'=>'Image uploaded successfully','image'=>$img]);
            else
                return response()->json(['status'=>false,'message'=>'Something went wrong, try again later!']);   
        }
        else{
            return response()->json(['status'=>false,'message'=>'Please upload file!']);
        }*/
        
    }
    
    public function customerregister(Request $request){
        if($request['user_type']=='customer'){
            $validator  = Validator::make($request->all(),[
                'email'=>'required|email',
                'name'=>'required',
                'password'=>'required',
                'mobile_no'=>'required',
                'address'=>'required',
                'gender'=>'required',
                'district'=>'required',
                'state'=>'required',
            ]);
            if($validator->fails()){
                return response()->json(['status'=>0,'message'=>$validator->errors()->first()]);
            }
            $check = User::where('email',$request['email'])->first();
            if($check){
                return response()->json(['status'=>0,'message'=>'User is already registered with this email!']);
            }
            else{
                $check2 = User::where('mobile_no',$request['mobile_no'])->first();
                if($check2){
                    return response()->json(['status'=>0,'message'=>'User is already registered with this mobile number!']);
                }
                else{
                    $localcart = new LocalCart();
                    $sendSMS = new SendSMS();
                    $otp = $localcart->generateOtp();
                    
                    $input['name']  = $request['name'];
                    $input['email'] = $request['email'];
                    $input['password'] = Hash::make($request['password']);
                    $input['decoded_password'] = base64_encode($request['password']);
                    $input['mobile_no'] = $request['mobile_no'];
                    if(isset($request['aadhar_no']))
                        $input['aadhar_no'] = $request['aadhar_no'];
                    if(isset($request['profile_pic']) && $request['profile_pic']!=NULL){
                        $img = $this->fileUpload($request['profile_pic'],'user_profile');
                        if($img){
                            $input['profile_pic'] = $img;
                        }
                    }
                        
                    $input['user_type'] = $request['user_type'];
                    $input['gender'] = $request['gender'];
                    $input['status'] = '1';
                    $input['otp'] = $otp;
                    $input['expire_otp'] = now();
                    $user  =  User::create($input);	 

                    $inp_address['user_id'] = $user->id;
                    $inp_address['address'] = $request['address'];
                    if(isset($request['type']))
                        $inp_address['type'] = $request['type'];
                    $inp_address['state'] = $request['state'];
                    $inp_address['district'] = $request['district'];
                    //   // $inp_address['longitude'] = $request['longitude'];
                    $address = Addresses::create($inp_address);
                    $sendSMS->send_message($user->mobile_no,$otp);
                    $success['user_details'] = $user;
                    $success['address_details'] = $address;
                    $success['user_details']['token'] = $user->createToken('MyApp')-> accessToken;
                    
                    if(isset($request['profile_pic'])){
                        $img_url = 'http://globizcloudserver.com/localcart/public/user_profile/'.$user->profile_pic;
                        $success['user_details']['pic_url'] = $img_url;
                    }
                    
                    if(count($success))
                        return response()->json(['status'=>1,'message'=>'Congratulations, You are registered successfully.','data'=>$success]);  
                    else
                        return response()->json(['status'=>0,'message'=>'Something went wrong, try again later!']);
                }   
                
            }
        }
        else{
            return response()->json(['status'=>0,'message'=>'User type must be customer!']);
        }
        
        
    }

    public function fileUpload($pic,$t){
        $file = new FileUpload();
        $type = $t;
        $img_name = $file->handle_base64_image_upload($pic,$type);
        
        if($img_name)
            return $img_name;
        else
            return false;
        
    }

    

    public function details() 
    { 
        if(Auth::user()){
            if(Auth::user()->user_type=='customer')
                $user = User::getCustomer(Auth::user()->id); 
            else
                $user = User::getShopkeeper(Auth::user()->id);
            return response()->json(['status'=>true,'message'=>'Customer details fetched, successfully!','success' => $user]); 
        }
        else
            return response()->json(['status'=>false,'message'=>'Something went wrong, try again later!']);
    }

    public function forgotpassword(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
        ]);
        $user = User::where('email', $request->email)->first();
        // dd($user);
        if (!$user)
            return response()->json([
                'message' => "We can't find a user with that e-mail address."
            ], 404);
        $passwordReset = PasswordReset::updateOrCreate(
            ['email' => $user->email],
            [
                'email' => $user->email,
                'token' => str_random(60)
             ]
        );
        if ($user && $passwordReset)
            $user->notify(
                new PasswordResetRequest($passwordReset->token)
            );
        return response()->json([
            'message' => 'We have e-mailed your password reset link!'
        ]);
    }

    public function find($token)
    {
        $passwordReset = PasswordReset::where('token', $token)
            ->first();
        if (!$passwordReset)
            return response()->json([
                'message' => 'This password reset token is invalid.'
            ], 404);
        if (Carbon::parse($passwordReset->updated_at)->addMinutes(720)->isPast()) {
            $passwordReset->delete();
            return response()->json([
                'message' => 'This password reset token is invalid.'
            ], 404);
        }
        return response()->json($passwordReset);
    }

    public function resetpassword(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string|confirmed',
            'token' => 'required|string'
        ]);
        $passwordReset = PasswordReset::where([
            ['token', $request->token],
            ['email', $request->email]
        ])->first();
        if (!$passwordReset)
            return response()->json([
                'message' => 'This password reset token is invalid.'
            ], 404);
        $user = User::where('email', $passwordReset->email)->first();
        if (!$user)
            return response()->json([
                'message' => "We can't find a user with that e-mail address."
            ], 404);
        $user->password = bcrypt($request->password);
        $user->save();
        $passwordReset->delete();
        $user->notify(new PasswordResetSuccess($passwordReset));
        return response()->json($user);
    }

    public function getstates(){
        $states = States::where('country_id','101')->select('id','name')->get();
        if(count($states)){
            return response()->json(['status'=>true,'message'=>'States fetched successfully!','data'=>$states]);
        }
        else{
            return response()->json(['status'=>false,'message'=>'No state found!']);
        }
    }

    public function getcities($id){
        $cities = Cities::where('state_id',$id)->select('id','name')->get();
        if(count($cities)){
            return response()->json(['status'=>true,'message'=>'Cities fetched successfully!','data'=>$cities]);
        }
        else{
            return response()->json(['status'=>false,'message'=>'No city found!']);
        }
    }

    public function getcontent($type){
        if($type=='faq'){
            $content = Faqs::where('status',1)->get();
        }
        else if($type=='tc'){
            $content = Contents::where('id',2)->first();
        }
        else if($type=='aboutus'){
            $content = Contents::where('id',3)->first();
        }
        else if($type=='privacy'){
            $content = Contents::where('id',4)->first();
        }
        else{
            return response()->json(['status'=>false,'message'=>'Please check type!']);
        }
        return response()->json(['status'=>true,'data'=>$content]);
    }
    
    public function updateProfile(Request $request){
        if(Auth::user()->user_type=="customer"){
            $validator  = Validator::make($request->all(),[
                'name'=>'required',
                'address'=>'required',
                'gender'=>'required',
                'district'=>'required',
                'state'=>'required',
            ]);
            if($validator->fails()){
                return response()->json(['status'=>0,'message'=>$validator->errors()->first()]);
            }
            $data = $request->all();
            $arr['name'] = $data['name'];
            $arr['email'] = $data['email'];
            $arr['mobile_no'] = $data['mobile_no'];
            if(isset($data['aadhar_no']))
                $arr['aadhar_no'] = $data['aadhar_no'];
            $arr['gender'] = $data['gender'];
            $upd = User::where('id',Auth::user()->id)->update($arr);
            if($upd){
                $arr2['address'] = $data['address'];
                $arr2['district'] = $data['district'];
                $arr2['state'] = $data['state'];
                $upd = Addresses::where('user_id',Auth::user()->id)->update($arr2);
                $user = User::getCustomer(Auth::user()->id);
                if($upd){
                    return response()->json(['status'=>true,'message'=>'Updated successfully!','data'=>$user]);
                }
                else{
                    return response()->json(['status'=>false,'message'=>'Something went wrong, try again!']);
                }
            }
            else{
                return response()->json(['status'=>false,'message'=>'Something went wrong, try again!']);
            }
        }
        else{
            $validator  = Validator::make($request->all(),[
                'name'=>'required',
                'address'=>'required',
                'gender'=>'required',
                'district'=>'required',
                'state'=>'required',
                'shop_name'=>'required',
                'shop_open_time'=>'required',
                'shop_close_time'=>'required',
                'bank_name' => 'required',
                'account_no'=>'required',
                'category_id'=>'required',
            ]);
            if($validator->fails()){
                return response()->json(['status'=>0,'message'=>$validator->errors()->first()]);
            }
            $data = $request->all();
            $arr['name'] = $data['name'];
            $arr['email'] = $data['email'];
            $arr['mobile_no'] = $data['mobile_no'];
            if(isset($data['aadhar_no']))
                $arr['aadhar_no'] = $data['aadhar_no'];
            $arr['gender'] = $data['gender'];
            $upd = User::where('id',Auth::user()->id)->update($arr);
            if($upd){
                $arr2['address'] = $data['address'];
                $arr2['district'] = $data['district'];
                $arr2['state'] = $data['state'];
                $upd2 = Addresses::where('user_id',Auth::user()->id)->update($arr2);
                if($upd2){
                    $arr3['bank_name'] = $data['bank_name'];
                    $arr3['account_no'] = $data['account_no'];
                    $upd3 = BankDetails::where('user_id',Auth::user()->id)->update($arr3);
                    if($upd3){
                        $arr4['shop_name'] = $data['shop_name'];
                        $arr4['shop_close_time'] = $data['shop_close_time'];
                        $arr4['shop_open_time'] = $data['shop_open_time'];
                        $arr4['category_id'] = $data['category_id'];
                        $upd4 = ShopDetails::where('user_id',Auth::user()->id)->update($arr4);
                        if($upd4){
                            $user = User::getShopkeeper(Auth::user()->id);
                            return response()->json(['status'=>true,'message'=>'Updated successfully','data'=>$user]);
                        }
                        else
                            return response()->json(['status'=>false,'message'=>'Something went wrong,while updating Shop details!']);
                    }
                    else
                        return response()->json(['status'=>false,'message'=>'Something went wrong,while updating Bank details!']);
                }
                else
                    return response()->json(['status'=>false,'message'=>'Something went wrong, while updating user address details!']);
            }
            else{
                return response()->json(['status'=>false,'message'=>'Something went wrong, while updating user details!']);
            }
           
            
        }
    }

    public function testMessage(){
        $sendSMS = new SendSMS();
        $sendSMS->send_message("9888222370","25256");
        
    }
    

    
}
