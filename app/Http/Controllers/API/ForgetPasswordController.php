<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Hash;
use File;
use App\User;
use App\Libraries\LocalCart;
use App\Libraries\SendSMS;
use Carbon\Carbon;

class ForgetPasswordController extends Controller
{
    public function forgotpassword(Request $request)
    {

        $validator  = Validator::make($request->all(), [
            'mobile_no' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        }
        $user = User::where('mobile_no', $request->mobile_no)->first();
        if (!$user)
            return response()->json(['status' => false, 'message' => "We can't find a user with this mobile number."]);
        $localcart = new LocalCart();
        $otp = $localcart->generateOtp();
        $token = $user->createToken('MyApp')->accessToken;
        $SendSMS = new SendSMS();
        $check = $SendSMS->send_message($request['mobile_no'],$otp);
        if($check){
            $passwordReset = User::where('id',$user->id)->update(['otp' => $otp,'expire_otp'=>now()]);
            $mob['otp'] = $otp;
            $mob['token'] = $token;
            if ($user && $passwordReset)
                return response()->json(['status' => true, 'message' => 'We have text you an otp!', 'data' => $mob]);
        }
        else{
            return response()->json(['status'=>false,'message'=>'Something went wrong try again later!']);
        }
    }

    public function verifyOtp(Request $request)
    {
        $validator  = Validator::make($request->all(), [
            'otp' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        }
        $data = $request->all();

        $otp_verifed = User::where('id',Auth::user()->id)->where('otp', $data['otp'])->first();
        if (!$otp_verifed)
            return response()->json(['status' => false, 'message' => 'This password reset otp is invalid.']);

        if (Carbon::parse($otp_verifed->expire_otp)->addMinutes(10)->isPast()) {
            User::where('id',Auth::user()->id)->update(['otp'=>NULL,'expire_otp'=>NULL]);
            return response()->json([
                'status' => false,
                'message' => 'The otp is expired, please try again!'
            ]);
        }
        $upd = User::where('id',Auth::user()->id)->update(['otp'=>NULL,'expire_otp'=>NULL]);
        if($upd){
            $a = User::where('id',Auth::user()->id)->first();
            return response()->json(['status' => true, 'message' => 'Otp verified successfully!', 'data' => $a]);
        }
        else{
            return response()->json(['status'=>false,'message'=>'Something went wrong, try again later!']);
        }
        
    }

    public function resetpassword(Request $request)
    {
        $validator  = Validator::make($request->all(), [
            'password' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        }
        $data = $request->all();
    
        $user = User::where('id', Auth::user()->id)->first();
        if (!$user)
            return response()->json([
                'status' => false,
                'message' => "User not found!"
            ]);
        $user->password = Hash::make($data['password']);
        $user->save();
        $t = Auth::user()->token()->revoke();
        $user['token'] = $user->createToken('MyApp')->accessToken;
        if($user){
            return response()->json(['status' => true, 'message' => 'password reset successfully!', 'data' => $user]);
        }
        else{
            return response()->json(['status'=>false,'message'=>'Something went wrong,try again later!']);
        }
        
        
    }

    public function resendOtp(){
        $localcart = new LocalCart();
        $otp =  $localcart->generateOtp();
        $SendSMS = new SendSMS();
        $check = $SendSMS->send_message(Auth::user()->mobile_no,$otp);
        if($check){
            $upd = User::where('id',Auth::user()->id)->update(['otp'=>$otp,'expire_otp'=>now()]);
            if($upd)
                return response()->json(['status'=>true,'message'=>'Otp has been sent to your registered mobile number!']);
            else
                return response()->json(['status'=>false,'message'=>'Something went wrong, Try again later!']);
        }
        else
            return response()->json(['status'=>false,'message'=>'Something went wrong, try again later!']);
        
    }
}