<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Hash;
use Session;
use App\User;
use App\Admin;
use App\Addresses;
use App\BankDetails;
use App\Options;
use App\Orders;
use App\Category;
use App\ShopDetails;
use App\PasswordReset;
use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;
use App\OrderItems;
use Mail;
use App\Faqs;
use App\Contents;

class AdminController extends Controller
{
    public function login(Request $request)
    {
        $logo = Options::where('key', "logo")->first();
        if ($request->isMethod('post')) {
            $data = $request->all();
            $any = array('email' => $data['email'], 'password' => $data['password']);
            if (Auth::guard('admin')->attempt($any)) {
                Session::flash('success', 'Login Successfully.');
                return Redirect('/dashboard');
            } else {
                Session::flash('error', 'Please enter valid credentails.');
                return Redirect('/');
            }
        }
        if (Auth::guard('admin')->user())
            return redirect('/dashboard');
        else
            return view('admin.login', ['logo' => $logo]);
    }

    public function dashboard()
    {
        return view('admin.dashboard');
    }

    public function register(Request $request)
    {
        $data = $request->all();
        $data['password'] = Hash::make($data['password']);
        $usr = Admin::create($data);
        if ($usr)
            return response()->json(['message' => 'created']);
        else
            return response()->json(['message' => 'error']);
    }

    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect('/');
    }

    public function file_extension($filename)
    {
        $img = explode('.', $filename);
        return $img[count($img) - 1];
    }

    public function profile(Request $request)
    {
        $admin = Admin::where('id', Auth::guard('admin')->user()->id)->first();
        if ($request->isMethod('POST')) {
            $image = NULL;
            if (isset($request['image'])) {
                if (is_uploaded_file($request['image'])) {
                    $file = $request['image'];
                    $ext = $this->file_extension($file->getClientOriginalName());
                    if ($ext == "png" || $ext == "jpg" || $ext == "jpeg") {
                        $img_name = date('Y_m_d_H:i:s') . '_' . $file->getClientOriginalName();
                        $file->move('./public/admin_profile/', $img_name);
                        $image = $img_name;
                    } else {
                        Session::flash('error', 'Only jpg, jpeg or png image allowed!');
                        return redirect()->back();
                    }

                    // $img_name = date('Y_m_d_H:i:s').'_'.$file->getClientOriginalName();
                    // $file->move('./public/'.$type.'/',$img_name);
                } else {
                    Session::flash('error', 'Only jpg, jpeg or png file are allowed!');
                    return redirect()->back();
                }
            }
            if ($image != NULL) {
                $arr['profile_pic'] = $image;
            }
            $arr['name'] = $request["name"];
            $arr['email'] = $request["email"];
            $upd = Admin::where('id', Auth::guard('admin')->user()->id)->update($arr);
            if ($upd)
                Session::flash('success', 'Admin details updated successfully!');
            else
                Session::flash('error', 'Something went wrong, try again later!');

            return redirect()->back();
        }
        return view('admin.profile')->with(['data' => $admin]);
    }

    public function customers()
    {
        $users = User::select('id', 'name', 'email', 'mobile_no', 'gender', 'status')->where('user_type', 'customer')->orderBy('created_at', 'DESC')->paginate(10);
        // dd($data);
        return view('admin.customers', compact('users'));
    }

    public function viewCustomer($id, Request $request)
    {
        $user_id = base64_decode($id);
        $user = User::where('id', $user_id)->with('address_details')->first();

        return view('admin.view_customer', ['user' => $user]);
    }

    public function customersFetchData(Request $request)
    {
        if ($request->ajax()) {
            $sort_by = $request->get('sortby');
            $sort_type = $request->get('sorttype');
            $query = $request->get('query');
            $query = str_replace(" ", "%", $query);
            $user_type = $request->get('type');

            $users = User::select('id', 'name', 'user_type', 'email', 'mobile_no', 'status')
                ->where('user_type', $user_type)
                ->where(function ($queryobj) use ($query) {
                    $queryobj->where('name', 'LIKE', '%' . $query . '%')
                        ->orWhere('email', 'LIKE', '%' . $query . '%')
                        ->orWhere('mobile_no', 'LIKE', '%' . $query . '%');
                })
                ->orderBy($sort_by, $sort_type)
                ->paginate(10);

            if ($user_type == "customer")
                return view('admin.tables.customer_data', compact('users'));
            else if ($user_type == 'shopkeeper')
                return view('admin.tables.shopkeeper_data', compact('users'));
        }
    }

    public function shopkeeper()
    {
        $data = User::select('id', 'name', 'email', 'mobile_no', 'gender', 'status')
            ->with(
                [
                    'shop_details' => function ($query) {
                        $query->select('user_id', 'category_id', 'shop_name')->get();
                    },
                    'address_details' => function ($query) {
                        $query->select('id', 'user_id', 'address', 'state', 'district')->with(['state_details', 'city_details'])->get();
                    }
                ]
            )
            ->where('user_type', 'shopkeeper')
            ->orderBy('created_at', 'DESC')
            ->paginate(10);
        return view('admin.shopkeepers', ['users' => $data]);
    }

    public function miscellaneous(Request $request)
    {
        $data = Options::where('key', '!=', 'logo')->get();
        if ($request->isMethod('POST')) {
            $arr['text'] = $request['modal_edit_option_text'];
            $arr['value'] = $request['modal_edit_option_value'];
            $upd = Options::where('id', $request['modal_edit_option_id'])->update($arr);
            if ($upd) {
                Session::flash('success', 'Updated successfully!');
            } else {
                Session::flash('error', 'Something went wrong, try again later!');
            }
            return redirect()->back();
        }
        return view('admin.miscellaneous', ['data' => $data]);
    }

    public function updatestatus(Request $request)
    {
        if ($request->isMethod('POST')) {
            $data = $request->all();
            if ($data['status'] == "true")
                $status = '1';
            else
                $status = '0';
            $upd = User::where('id', $data['user_id'])->update(['status' => $status]);
            if ($upd) {
                return response()->json(['status' => true, 'message' => 'User status updated, successfully!']);
            } else {
                return response()->json(['status' => false, 'message' => 'Something went wrong, try again later!']);
            }
        } else {
            return response()->json(['status' => false, 'message' => 'request method should be post!']);
        }
    }

    public function deleteuser(Request $request)
    {
        if ($request['type'] == 'customer') {
            $check = Orders::where('user_id', $request['user_id'])->count();
            if ($check) {
                return response()->json(['status' => false, 'message' => "This user has active orders, you can't delete this!"]);
            }

            $user = User::where('id', $request['user_id'])->with('address_details')->delete();
            if ($user) {
                return response()->json(['status' => true, 'message' => 'Customer deleted successfully!']);
            } else {
                return response()->json(['status' => false, 'message' => 'Something went wrong, try again later!']);
            }
        } else if ($request['type'] == 'shopkeeper') {
            $shopDetail = ShopDetails::where('user_id', $request['user_id'])->first();
            $check = Orders::where('shop_id', $shopDetail->id)->count();
            if ($check) {
                return response()->json(['status' => false, 'message' => "This user have active order,you can't delete this!"]);
            }
            $user = User::where('id', $request['user_id'])->with(['shop_details', 'address_details', 'bank_details'])->delete();


            if ($user) {
                return response()->json(['status' => true, 'message' => 'Shopkeeper deleted successfully!']);
            } else {
                return response()->json(['status' => false, 'message' => 'Something went wrong, try again later!']);
            }
        }
    }

    public function categories(Request $request)
    {
        $data = Category::select('id', 'category_name')->paginate(10);
        if ($request->isMethod('POST')) {
            $arr['category_name'] = $request['modal_category_name'];
            if (Category::insert($arr))
                Session::flash('success', 'Category inserted successfully!');
            else
                Session::flash('error', 'Something went wrong,try again later!');
            return redirect()->back();
        }
        return view('admin.categories', ['data' => $data]);
    }

    public function deletecategory(Request $request)
    {
        if ($request->isMethod('POST')) {
            $del = Category::where('id', $request['category_id'])->delete();
            if ($del) {
                return response()->json(['status' => true, 'message' => 'Category deleted successfully!']);
            } else {
                return response()->json(['status' => false, 'message' => 'Something went wrong, try again later!']);
            }
        } else {
            return response()->json(['status' => false, 'message' => 'Request method must be post!']);
        }
    }

    public function orders(Request $request)
    {
        $orders = Orders::select('id', 'shop_id', 'user_id', 'pickup_time', 'notes', 'status')->with([
            'customer_details' => function ($query) {
                $query->select('id', 'name', 'email', 'mobile_no');
            },
            'shop_details' => function ($query) {
                $query->select('shop_name', 'id', 'user_id')->with(['shopkeeper_details' => function ($query1) {
                    $query1->select('id', 'name', 'email', 'mobile_no');
                },]);
            },
            'order_item' => function ($query) {
                $query->select('id', 'order_id', 'item_name', 'weight', 'quantity', 'comment');
            }
        ])->orderBy('created_at', 'DESC')->paginate(15);
        if ($request->ajax()) {
            $query = $request->get('query');
            $orders = Orders::with(['customer_details', 'shop_details', 'shop_details.shopkeeper_details'])
                ->whereHas('customer_details', function ($queryObj) use ($query) {
                    $queryObj->where('name', 'LIKE', '%' . $query . '%')
                        ->orWhere('mobile_no', 'LIKE', '%' . $query . '%');
                })
                ->orWhereHas('shop_details', function ($queryObj) use ($query) {
                    $queryObj->where('shop_name', 'LIKE', '%' . $query . '%');
                })
                ->orWhereHas('shop_details.shopkeeper_details', function ($queryObj) use ($query) {
                    $queryObj->where('name', 'LIKE', '%' . $query . '%')
                        ->orWhere('mobile_no', 'LIKE', '%' . $query . '%');
                })
                ->paginate(15);

            return view('admin.tables.orders_data', compact('orders'));
        }
        return view('admin.orders', compact('orders'));
    }


    public function changepassword(Request $request)
    {

        $validator  = Validator::make($request->all(), [
            'old_password' => 'required',
            'new_password' => 'required',
            'confirm_password' => 'required|same:new_password',
        ]);
        if ($validator->fails()) {
            Session::flash('error', $validator->errors()->first());
            return redirect()->back();
        }

        $old_password = $request['old_password'];

        if ($old_password == base64_decode(Auth::guard('admin')->user()->decoded_password)) {
            $new_password = base64_encode($request['new_password']);
            $hash_password = Hash::make($request['new_password']);
            $upd = Admin::where('id', Auth::guard('admin')->user()->id)->update(['decoded_password' => $new_password, 'password' => $hash_password]);
            if ($upd) {
                Session::flash('success', 'Password updated successfully!');
            } else {
                Session::flash('error', 'Something went wrong, try again later!');
            }
            return redirect()->back();
        } else {
            Session::flash('error', "Old Password doesn't match");
        }
        return redirect()->back();
    }

    /*public function forgotpassword(Request $request)
    {
        if($request->isMethod('post')){
            $request->validate([
                'email' => 'required|string|email',
            ]);
            $user = Admin::where('email', $request->email)->first();
            if (!$user)
            {
                Session::flash('error',"We can't find a user with that e-mail address.");
                return redirect()->back();
            }
            $passwordReset = PasswordReset::updateOrCreate(
                ['email' => $user->email],
                [
                    'email' => $user->email,
                    'token' => str_random(60)
                 ]
            );
            if ($user && $passwordReset){
               
            }
               
            Session::flash('success',"We have e-mailed your password reset link!");
            return response()->back();
        }
        return view('admin.forgot_password');
    }*/

    public function forgotPassword(Request $request)
    {
        if ($request->isMethod('post')) {
            $post_data  =  $request->all();
            $email      =  $post_data['email'];
            $email_data =  Admin::where(['email' => $email])->first();
            if ($email_data) {
                $encoded_admin_id  =  base64_encode($email_data['id']);
                $random_number     =  rand();
                $message  =  [
                    'name'           =>  $email_data['name'],
                    'id'             =>  $encoded_admin_id,
                    'random_number'  =>  $random_number
                ];
                Mail::send('email.forgot_password', $message, function ($message) use ($email) {
                    $message->to($email)->subject('Forgot Password');
                });
                Admin::where(['id' => $email_data['id']])->update(['reset_password' => $random_number]);
                Session::flash('success', 'Your forgot password link send to your registered email address !!');
                return Redirect('/');
            } else {
                Session::flash('error', 'This email does not exist !!');
                return Redirect()->back();
            }
        }
        return view('admin.forgot_password');
    }

    public function resetPassword(Request $request, $id = null, $random_number = null)
    {
        $admin_id      =  base64_decode($id);
        $admin_data    =  Admin::where(['id' => $admin_id])->first();
        if ($admin_data['reset_password'] == $random_number) {
            return view('admin.reset_password')->with(['admin_id' => $admin_id, 'random_number' => $random_number]);
        } else {
            Session::flash('error', 'Your Link has been expired please send again !!');
            return view('admin.link_expire');
        }
    }

    public function update_password(Request $request)
    {
        if ($request->isMethod('post')) {
            $post_data   =  $request->all();
            $admin_id    =  $post_data['admin_id'];
            $admin_data  =  Admin::where(['id' => $admin_id])->first();
            if ($admin_data['reset_password'] == $post_data['random_number']) {
                $update_data['password']  = Hash::make($post_data['password']);
                $update_data['decoded_password'] = base64_encode($post_data['password']);
                $update_data['reset_password'] = 0;
                if (Admin::where(['id' => $admin_id])->update($update_data)) {
                    Session::flash('success', 'Password reset successfully !!');
                    return view('admin.reset_thanku');
                } else {
                    Session::flash('error', 'Password could not be change.Please try again !!');
                    return view('admin.reset_error');
                }
            } else {
                Session::flash('error', 'Your Link has been expired please send again !!');
                return view('admin.link_expire');
            }
        } else {
            Session::flash('error', 'Password could not be change.Please try again !!');
            return view('admin.reset_error');
        }
    }

    public function updatecategory(Request $request)
    {
        if ($request->isMethod('post')) {
            $id = $request['modal_edit_category_id'];
            $category_name = $request['modal_edit_category_name'];
            $upd = Category::where('id', $id)->update(['category_name' => $category_name]);
            if ($upd) {
                Session::flash('success', 'Category updated successfully!');
            } else {
                Session::flash('error', 'Something went wrong,try again later!');
            }
            return redirect()->back();
        }
    }

    public function editorderstatus(Request $request)
    {
        $order_id = $request['modal_edit_order_id'];
        $status = $request['modal_edit_order_status'];
        $upd = Orders::where('id', $order_id)->update(['status' => $status]);
        if ($upd) {
            Session::flash('success', 'Order status updated successfully!');
        } else {
            Session::flash('error', 'Something went wrong,try again later!');
        }
        return redirect()->back();
    }

    public function editorderitemstatus(Request $request)
    {
        $item_id = $request['modal_edit_item_id'];
        $status = $request['modal_edit_item_status'];
        $upd = OrderItems::where('id', $item_id)->update(['status' => $status]);
        if ($upd) {
            Session::flash('success', 'Order status updated successfully!');
        } else {
            Session::flash('error', 'Something went wrong,try again later!');
        }
        return redirect()->back();
    }

    public function vieworder($id, Request $request)
    {
        $order_id = base64_decode($id);
        $order_details = Orders::select('id', 'shop_id', 'user_id', 'pickup_time', 'store_owner_note', 'notes', 'status')
            ->where('id', $order_id)
            ->with([
                'customer_details' => function ($query) {
                    $query->select('id', 'name', 'email', 'mobile_no');
                },
                'shop_details' => function ($query) {
                    $query->select('shop_name', 'id', 'user_id')->with(['shopkeeper_details' => function ($query1) {
                        $query1->select('id', 'name', 'email', 'mobile_no');
                    },]);
                },
                'order_item' => function ($query) {
                    $query->select('id', 'order_id', 'item_name', 'weight', 'status', 'quantity', 'comment')->get();
                }
            ])->orderBy('created_at', 'DESC')->first();
        if ($request->isMethod('POST')) {
        }
        // dd($order_details);
        return view('admin.vieworder', ['order_details' => $order_details]);
    }

    public function deleteorder(Request $request)
    {
        $order_id = $request['order_id'];
        $del_items = OrderItems::where('order_id', $order_id)->delete();
        if ($del_items) {
            $del = Orders::where('id', $order_id)->delete();
            if ($del) {
                return response()->json(['status' => true, 'message' => 'Order deleted successfully!']);
            } else {
                return response()->json(['status' => false, 'message' => 'Something went wron,try again later!']);
            }
        } else {
            return response()->json(['status' => false, 'message' => 'Something went wron,try again later!']);
        }
        return redirect()->back();
    }

    public function faq(Request $request)
    {
        $content = Faqs::get();
        if ($request->isMethod('POST')) {
            $faq_id = $request['faq_id'];
            $arr['question'] = $request['question'];
            $arr['answer'] = $request['answer'];
            $upd = Faqs::where('id', $faq_id)->update($arr);
            if ($upd) {
                Session::flash('success', 'Updated successfully!');
            } else {
                Session::flash('error', 'Something went wrong,try again later!');
            }
            return redirect()->back();
        }
        return view('admin.faq', ['data' => $content]);
    }

    public function addFaq(Request $request)
    {
        $arr = [
            'question' => $request['add_question'],
            'answer' => $request['add_answer'],
        ];
        $ins = Faqs::create($arr);
        if ($ins)
            Session::flash('success', 'Inserted successfully!');
        else
            Session::flash('error', 'Something went wrong,try again later!');
        return redirect()->back();
    }

    public function deleteFaq(Request $request)
    {
        $faq_id = $request->get('faq_id');
        $del = Faqs::where('id', $faq_id)->delete();
        if ($del) {
            return response()->json(['status' => true, 'message' => 'Deleted successfully!']);
        } else {
            return response()->json(['status' => false, 'message' => 'Something went wrong, try again later!']);
        }
    }

    public function termsconditions(Request $request)
    {
        $content = Contents::where('id', 2)->first();
        if ($request->isMethod('POST')) {
            $arr['slug'] = $request['modal_t&c_slug'];
            $arr['title'] = $request['modal_t&c_title'];
            $arr['content'] = $request['editor1'];
            $upd = Contents::where('id', 2)->update($arr);
            if ($upd) {
                Session::flash('success', 'Updated successfully!');
            } else {
                Session::flash('error', 'Something went wrong,try again later!');
            }
            return redirect()->back();
        }
        return view('admin.terms&conditions', ['data' => $content]);
    }

    public function aboutus(Request $request)
    {
        $content = Contents::where('id', 3)->first();
        if ($request->isMethod('POST')) {
            $arr['slug'] = $request['modal_about_slug'];
            $arr['title'] = $request['modal_about_title'];
            $arr['content'] = $request['editor1'];
            $upd = Contents::where('id', 3)->update($arr);
            if ($upd) {
                Session::flash('success', 'Updated successfully!');
            } else {
                Session::flash('error', 'Something went wrong,try again later!');
            }
            return redirect()->back();
        }
        return view('admin.aboutus', ['data' => $content]);
    }

    public function privacypolicy(Request $request)
    {
        $content = Contents::where('id', 4)->first();
        if ($request->isMethod('POST')) {
            $arr['slug'] = $request['modal_about_slug'];
            $arr['title'] = $request['modal_about_title'];
            $arr['content'] = $request['editor1'];
            $upd = Contents::where('id', 4)->update($arr);
            if ($upd) {
                Session::flash('success', 'Updated successfully!');
            } else {
                Session::flash('error', 'Something went wrong,try again later!');
            }
            return redirect()->back();
        }
        return view('admin.privacypolicy', ['data' => $content]);
    }
}
