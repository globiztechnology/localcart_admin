<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Admin extends Authenticatable
{
    use Notifiable;

    protected $guard = 'admin';

    protected $table = "admins";
    
    protected $fillable = [
        'name','email','password','decoded_password','profile_pic','is_super','reset_password','created_at','updated_at'
    ];

    protected $hidden = ['password','remember_token'];
}
