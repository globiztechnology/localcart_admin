<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Notifications extends Authenticatable
{
    use Notifiable;

    protected $table = "notification";
    
    protected $fillable = [
        'from_user_id','to_user_id','order_id','shop_id','type','message','created_at','updated_at'
    ];

}
