<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class States extends Authenticatable
{
    use Notifiable;

    // protected $guard = 'admin';

    protected $table = "states";
    
    protected $fillable = [
        'country_id','name'
    ];

    public function state_country(){
        return $this->hasOne('App\Countries','id','country_id');
    }

    public function state_destricts(){
        return $this->belongsTo('App\Cities','state_id','id');
    }

}
