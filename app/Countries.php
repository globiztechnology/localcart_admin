<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Countries extends Authenticatable
{
    use Notifiable;

    // protected $guard = 'admin';

    protected $table = "countries";
    
    protected $fillable = [
        'sortname','name','phonecode'
    ];

    public function country_states(){
        return $this->belongsTo('App\States','country_id');
    }

}
