<?php

namespace app\Libraries;

use Illuminate\Support\Collection;
use Carbon\Carbon;
use App\Queues;
use App\ShopDetails;
use App\Options;
use App\Notifications;
use App\DeviceToken;



class Notification
{

    public function insert_notification($from_user_id, $to_user_id, $order_id = NULL, $shop_id, $type, $message)

    {
        $arr['from_user_id'] = $from_user_id;
        $arr['to_user_id'] = $to_user_id;
        $arr['order_id'] = $order_id;
        $arr['shop_id'] = $shop_id;
        $arr['type'] = $type;
        $arr['message'] = $message;
        $create = Notifications::create($arr);
        if ($create) {
            // $this->trigger_notification($create);
            return true;
        } else
            return false;
    }
}
