<?php

namespace app\Libraries;

use app\Libraries\Coronawares;
use GuzzleHttp\Promise\Coroutine;
use Orchestra\Parser\Xml\Facade as XmlParser;

class SendSMS
{

    public function send_message($mobile_no, $otp)
    {
        $curl = curl_init();
        $apiKey = "TOhZ3IcrkPlX9abHVCdoqUj28v4A7pLtwSmQB5F1EynxJGR0zNGMveXd4tnqUEl9br0YKw3TOyzSAJ8k";
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://www.fast2sms.com/dev/bulk?authorization=".$apiKey."&sender_id=FSTSMS&language=english&route=qt&numbers=".$mobile_no."&message=33153&variables={BB}&variables_values=".$otp."",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return false;
        } else {
            return true;
        }
    }
}
