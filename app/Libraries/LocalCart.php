<?php 
namespace app\Libraries;

use App\Options;
use App\ShopDetails;

class LocalCart{

    public function generateOtp(){
        $alphabet = '1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 6; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass);
    }

    public function check_minimum_balance($user_wallet){
        $min_amt = Options::where('key','min_wallet_amt')->first();
        if($user_wallet>=$min_amt->value){
            return true;
        }
        return false;
    }

    public function checkShopStatus($shop_id){
        $check = ShopDetails::where('id',$shop_id)->pluck('status');
        return $check[0];
    }


}
