<?php
namespace app\Libraries;
use Carbon\Carbon;


class FileUpload{
    function handle_base64_image_upload($imageData,$u_type)
    {
        list($type, $imageData) = explode(';', $imageData);
        list(,$extension) = explode('/',$type);
        list(,$imageData)      = explode(',', $imageData);
        $fileName = date('Y_m_d_H:i:s').'.'.$extension;
        $imageData = base64_decode($imageData);
        $path = './public/'.$u_type.'/';
        file_put_contents($path.$fileName, $imageData);
        return file_exists($path. $fileName) ? $fileName : false;
    }
}

?>