<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Faqs extends Authenticatable
{
    use Notifiable;

    // protected $guard = 'admin';

    protected $table = "faqs";
    
    protected $fillable = [
        'question','answer','status','created_at','updated_at'
    ];


}
