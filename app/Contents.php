<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Contents extends Authenticatable
{
    use Notifiable;

    // protected $guard = 'admin';

    protected $table = "content";
    
    protected $fillable = [
        'title','slug','content','created_at','updated_at'
    ];

}
