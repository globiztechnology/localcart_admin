<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Boolean;

class ShopDetails extends Model
{
    protected $table = "shop_details";

    protected $fillable = [
        'user_id', 'category_id', 'shop_name', 'shop_open_time', 'shop_close_time', 'status', 'created_at', 'updated_at'
    ];

    public function getStatusAttribute($value){
        return (Boolean)$value;
    }

    public function category_details()
    {
        return $this->hasOne('App\Category', 'id', 'category_id');
    }

    public function address_details()
    {
        return $this->hasOne('App\Addresses', 'user_id', 'user_id');
    }

    public function shopkeeper_details()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public static function storeDetails($user_id = NULL)
    {
        if ($user_id) {
            return self::where('user_id', $user_id)->with(['address_details' => function ($query) {
                    $query->with(['state_details', 'city_details']);
                }, 'category_details'])->paginate(10);
        }
        return self::with(['address_details' => function ($query) {
            $query->with(['state_details', 'city_details']);
        }])->paginate(10);
    }

    public static function updateStatus($shop_id, $status)
    {
        return self::where('id', $shop_id)->update(['status' => $status]);
    }

    public static function singleStoreDetail($shop_id){
        return ShopDetails::with(['address_details', 'shopkeeper_details' => function ($query) {
            $query->select('id', 'name', 'email', 'mobile_no');
        }])
        ->where('id', $shop_id)->first();
    }
}
