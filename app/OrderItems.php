<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class OrderItems extends Authenticatable
{
    use Notifiable;

    // protected $guard = 'admin';

    protected $table = "order_item";
    
    protected $fillable = [
        'order_id','item_name','weight','weight_form','quantity','comment','status','created_at','updated_at'
    ];

}
