<?php

namespace App;
use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class OauthAccessToken extends Authenticatable
{
    use HasApiTokens,Notifiable; 

}
