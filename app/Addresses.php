<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Addresses extends Authenticatable
{
    use Notifiable;

    // protected $guard = 'admin';

    protected $table = "addresses";
    
    protected $fillable = [
        'user_id','type','address','district','state','latitude','longitude','created_at','updated_at'
    ];

    public function state_details(){
        return $this->hasOne('App\States','id','state');
    }

    public function city_details(){
        return $this->hasOne('App\Cities','id','district');
    }
}
