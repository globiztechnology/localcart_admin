<?php


Route::group(['middlewareGroups' => ['web']], function(){
    Route::match(['get','post'],'/','AdminController@login');
    Route::match(['get','post'],'/login','AdminController@login');
    Route::post('/register','AdminController@register');
    Route::match(['get','post'],'/forgot-password','AdminController@forgotpassword');
    Route::match(['get','post'],'/reset-password/{id}/{random_number}','AdminController@resetpassword');
    Route::post('/update-password','AdminController@update_password');
});
Route::group(['middleware' => 'admin'], function(){        
    Route::get('/details', 'AdminController@details');
    Route::get('/dashboard','AdminController@dashboard');
    Route::get('/logout','AdminController@logout');
    Route::match(['get','post'],'/profile','AdminController@profile');
    Route::get('/customers','AdminController@customers');
    Route::match(['get','post'],'/customer/view/{id}','AdminController@viewCustomer');
    Route::get('/customers/fetch-data','AdminController@customersFetchData');
    Route::match(['get','post'],'/store-owners','AdminController@shopkeeper');
    Route::match(['get','post'],'/update-status','AdminController@updatestatus');
    Route::post('/delete-user','AdminController@deleteuser');
    Route::match(['get','post'],'/categories','AdminController@categories');
    Route::post('/delete-category','AdminController@deletecategory');
    Route::match(['get','post'],'/orders','AdminController@orders');
    Route::post('/change-password','AdminController@changepassword');
    Route::post('/update-category','AdminController@updatecategory');
    Route::post('/edit-order-status','AdminController@editorderstatus');
    Route::match(['get','post'],'/view-order/{id}','AdminController@vieworder');
    Route::post('/delete-order','AdminController@deleteorder');
    Route::match(['get','post'],'/miscellaneous','AdminController@miscellaneous');
    Route::post('/edit-order-item-status','AdminController@editorderitemstatus');
    Route::match(['get','post'],'/faq','AdminController@faq');
    Route::get('/faq/delete','AdminController@deleteFaq');
    Route::post('faq/add','AdminController@addFaq');
    Route::match(['get','post'],'/terms&conditions','AdminController@termsconditions');
    Route::match(['get','post'],'/about-us','AdminController@aboutus');
    Route::get('/stores/{id}','BackendShopsController@storeownerstores');
    Route::match(['get','post'],'/stores','BackendShopsController@stores');
    Route::match(['get','post'],'states','RegionController@states');
    Route::match(['get','post'],'states/edit','RegionController@statesEdit');
    Route::match(['get','post'],'districts','RegionController@cities');
    Route::get('delete','RegionController@deleteItem');
    Route::match(['get','post'],'/privacy-policy','AdminController@privacypolicy');
});

