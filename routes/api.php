<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['api', 'cors']], function () {
    Route::get('send-message','API\AdminController@testMessage');
    Route::post('login', 'API\AuthenticationController@login');
    Route::post('register', 'API\AdminController@customerregister');
    Route::post('shopkeeper-register','API\ShopkeeperController@shopregister');
    Route::post('forget-password', 'API\ForgetPasswordController@forgotpassword');
    
    Route::get('get-categories','API\ShopsController@getCategories');
    Route::get('get-states','API\AdminController@getstates');
    Route::get('get-cities/{id}','API\AdminController@getcities');
    Route::group(['middleware' => ['auth:api','CheckUserStatus']], function(){
        Route::post('image-upload','API\AdminController@imageupload');
        Route::post('verify-otp','API\ForgetPasswordController@verifyOtp');
        Route::post('mobile-verification','API\AuthenticationController@mobileVerification');
        Route::get('resend-otp','API\ForgetPasswordController@resendOtp');
        Route::post('reset-password', 'API\ForgetPasswordController@resetpassword');
        Route::get('user-details', 'API\AdminController@details');
        Route::post('categories-shops','API\ShopsController@catgoriesShop');
        Route::post('shop-details','API\ShopsController@shop_details');
        Route::post('create-order','API\OrdersController@createOrder');
        Route::get('my-orders','API\OrdersController@myOrders');
        Route::post('order-details','API\OrdersController@orderDetails');
        Route::get('get-content/{type}','API\AdminController@getcontent');
        Route::post('change-order-status','API\OrdersController@changeorderstatus');
        Route::get('/my-shop-orders','API\OrdersController@shopOrders');
        Route::get('/orders-count','API\OrdersController@newOrderCount');
        Route::post('/change-multiple-order-status','API\OrdersController@changeMultipleOrderStatus');
        Route::post('update-profile','API\AdminController@updateProfile');
        Route::post('update-shop','API\ShopsController@updateShopStatus');
    });
});



